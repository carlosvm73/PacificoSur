'use strict';

describe('Controller: ConformidadCtrl', function () {

  // load the controller's module
  beforeEach(module('clientApp'));

  var ConformidadCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ConformidadCtrl = $controller('ConformidadCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ConformidadCtrl.awesomeThings.length).toBe(3);
  });
});
