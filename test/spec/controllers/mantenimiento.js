'use strict';

describe('Controller: MantenimientoCtrl', function () {

  // load the controller's module
  beforeEach(module('clientApp'));

  var MantenimientoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MantenimientoCtrl = $controller('MantenimientoCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MantenimientoCtrl.awesomeThings.length).toBe(3);
  });
});
