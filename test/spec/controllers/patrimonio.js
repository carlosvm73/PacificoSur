'use strict';

describe('Controller: PatrimonioCtrl', function () {

  // load the controller's module
  beforeEach(module('clientApp'));

  var PatrimonioCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PatrimonioCtrl = $controller('PatrimonioCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PatrimonioCtrl.awesomeThings.length).toBe(3);
  });
});
