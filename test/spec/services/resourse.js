'use strict';

describe('Service: resourse', function () {

  // load the service's module
  beforeEach(module('clientApp'));

  // instantiate service
  var resourse;
  beforeEach(inject(function (_resourse_) {
    resourse = _resourse_;
  }));

  it('should do something', function () {
    expect(!!resourse).toBe(true);
  });

});
