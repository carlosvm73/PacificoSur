CREATE DATABASE IF NOT EXISTS sa;
USE sa;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
CREATE TABLE IF NOT EXISTS proveedor(
  idProveedor VARCHAR(30) PRIMARY KEY ,
  primer_apellido_proveedor VARCHAR(20) NOT NULL,
  segundo_apellido_proveedor VARCHAR(20) NOT NULL,
  nombres_proveedor VARCHAR(30) NOT NULL,
  direccion_proveedor VARCHAR(60) NOT NULL,
  ruc_proveedor VARCHAR(10) UNIQUE
);
CREATE TABLE IF NOT EXISTS cliente(
  primer_apellido_cliente VARCHAR(20) NOT NULL,
  segundo_apellido_cliente VARCHAR(20) NOT NULL,
  nombres_cliente VARCHAR(30) NOT NULL,
  dni_cliente VARCHAR(8) PRIMARY KEY  NOT NULL
);
CREATE TABLE IF NOT EXISTS bien (
  idBien  INTEGER PRIMARY KEY AUTO_INCREMENT,
  descripcion_bien VARCHAR(500) NOT NULL,
  tipoTangintang VARCHAR(150) NOT NULL
);
CREATE TABLE IF NOT EXISTS cargo (
  idCargo  VARCHAR(20) PRIMARY KEY NOT NULL,
  descripcion_cargo VARCHAR(500) NOT NULL
);
CREATE TABLE IF NOT EXISTS area (
  idArea   VARCHAR(20) PRIMARY KEY NOT NULL,
  descripcion_area VARCHAR(500) NOT NULL
);
CREATE TABLE IF NOT EXISTS unidad (
  idUnidad   INTEGER PRIMARY KEY AUTO_INCREMENT,
  descripcion_unidad VARCHAR(500) NOT NULL
);


-- CREATE TABLE IF NOT EXISTS catpatrimonio (
--   idCatpatrimonio   INTEGER PRIMARY KEY AUTO_INCREMENT,
--   descripcion_catpatrimonio VARCHAR(500) NOT NULL
-- );
-- CREATE TABLE IF NOT EXISTS factorriesgo (
--   idFactorriesgo   INTEGER PRIMARY KEY AUTO_INCREMENT,
--   descripcion_factorriesgo VARCHAR(500) NOT NULL
-- );
-- CREATE TABLE IF NOT EXISTS vulnerabilidad (
--   idVulnerabilidad   INTEGER PRIMARY KEY AUTO_INCREMENT,
--   descripcion_vulnerabilidad VARCHAR(500) NOT NULL
-- );
-- CREATE TABLE IF NOT EXISTS tipobien (
--   idTipobien   INTEGER PRIMARY KEY AUTO_INCREMENT,
--   descripcion_tipobien VARCHAR(500) NOT NULL
-- );
-- CREATE TABLE IF NOT EXISTS tipoactivo (
--   idTipoactivo   INTEGER PRIMARY KEY AUTO_INCREMENT,
--   descripcion_tipoactivo VARCHAR(500) NOT NULL
-- );



CREATE TABLE IF NOT EXISTS personal (
  primer_apellido_personal VARCHAR(20) NOT NULL,
  segundo_apellido_personal VARCHAR(20) NOT NULL,
  nombres_personal VARCHAR(30) NOT NULL,
  dni_personal VARCHAR(8) PRIMARY KEY NOT NULL,
  idCargo  VARCHAR(20) NOT NULL,
  idArea  VARCHAR(20) NOT NULL,
  CONSTRAINT FOREIGN KEY (idCargo) REFERENCES cargo (idCargo) ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT FOREIGN KEY (idArea) REFERENCES area (idArea) ON UPDATE RESTRICT ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS usuario(
  codUsuario INTEGER PRIMARY KEY AUTO_INCREMENT ,
  user_name VARCHAR(40) NOT NULL,
  user_password VARCHAR(500) NOT NULL,
  user_type VARCHAR(15) NOT NULL,
  fechaCaduca datetime DEFAULT NULL,
  dni_personal VARCHAR(8) NOT NULL,
  CONSTRAINT FOREIGN KEY (dni_personal)  REFERENCES personal (dni_personal) ON UPDATE RESTRICT ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS actaconformidadservicio(
  nro_identificacion INTEGER PRIMARY KEY AUTO_INCREMENT,
  concepto varchar(500) DEFAULT NULL,
  nro_contrato decimal(10,0) NOT NULL,
  nro_os decimal(10,0) NOT NULL,
  nro_doc_ref decimal(10,0) NOT NULL,
  fecha_conformidad datetime DEFAULT NULL,
  dni_personal varchar(8) NOT NULL,
  CONSTRAINT FOREIGN KEY (dni_personal)  REFERENCES personal (dni_personal) ON UPDATE RESTRICT ON DELETE RESTRICT,
  dni_cliente varchar(8) NOT NULL,
  CONSTRAINT FOREIGN KEY (dni_cliente)  REFERENCES cliente  (dni_cliente) ON UPDATE RESTRICT ON DELETE RESTRICT,
  idProveedor VARCHAR(30) NOT NULL,
  CONSTRAINT FOREIGN KEY (idProveedor)   REFERENCES proveedor (idProveedor) ON UPDATE RESTRICT ON DELETE RESTRICT,
  estado decimal(10,0) NOT NULL,
  descripcion_acta varchar(500) NOT NULL,
  monto_registro_conformidad decimal(10,0) NOT NULL,
  monto_con_conformidad decimal(10,0) NOT NULL,
  monto_saldo decimal(10,0) NOT NULL,
  descripcion_servicio varchar(500) NOT NULL
);
CREATE TABLE IF NOT EXISTS requerimientobienes (
  idRequerimiento INTEGER PRIMARY KEY AUTO_INCREMENT ,
  fecha datetime DEFAULT NULL,
  numero_solicitud VARCHAR(20) NOT NULL,
  dni_personal varchar(8) NOT NULL,
  CONSTRAINT FOREIGN KEY (dni_personal)  REFERENCES personal (dni_personal) ON UPDATE RESTRICT ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS detalleRequerimiento (
  idDetalle INTEGER PRIMARY KEY AUTO_INCREMENT ,
  cantidad INTEGER NOT NULL,
  precio decimal(10,0) NOT NULL,
  idRequerimiento INTEGER NOT NULL,
  CONSTRAINT FOREIGN KEY (idRequerimiento)  REFERENCES requerimientobienes (idRequerimiento) ON UPDATE RESTRICT ON DELETE RESTRICT,
  idBien INTEGER NOT NULL,
  CONSTRAINT FOREIGN KEY (idBien)  REFERENCES bien (idBien) ON UPDATE RESTRICT ON DELETE RESTRICT,
  idUnidad INTEGER NOT NULL,
  CONSTRAINT FOREIGN KEY (idUnidad)  REFERENCES unidad (idUnidad) ON UPDATE RESTRICT ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS actapedido(
  idActa INTEGER PRIMARY KEY AUTO_INCREMENT ,
  fecha datetime DEFAULT NULL,
  motivo varchar(500) NOT NULL,
  tipo_uso varchar(500) NOT NULL,
  idArea VARCHAR(20) NOT NULL,
  CONSTRAINT FOREIGN KEY (idArea)  REFERENCES area (idArea) ON UPDATE RESTRICT ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS detallePedido(
  idDetalle INTEGER PRIMARY KEY AUTO_INCREMENT ,
  descripcion_detallePedido VARCHAR(500) NOT NULL,
  clasificador VARCHAR(500) NOT NULL,
  monto decimal(30,0) NOT NULL,
  idUnidad INTEGER NOT NULL,
  CONSTRAINT FOREIGN KEY (idUnidad)  REFERENCES unidad (idUnidad) ON UPDATE RESTRICT ON DELETE RESTRICT,
  idActa INTEGER NOT NULL,
  CONSTRAINT FOREIGN KEY (idActa)  REFERENCES actapedido (idActa) ON UPDATE RESTRICT ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS tarea(
  descripcion_tarea VARCHAR(500) NOT NULL,
  idTarea INTEGER PRIMARY KEY  AUTO_INCREMENT ,
  idActa INTEGER NOT NULL,
  CONSTRAINT FOREIGN KEY (idActa)  REFERENCES actapedido (idActa) ON UPDATE RESTRICT ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS mantenimiento (
  idMantenimiento INTEGER PRIMARY KEY AUTO_INCREMENT ,
  fecha datetime DEFAULT NULL,
  servicio VARCHAR(500) NOT NULL,
  descripcion_mantenimiento VARCHAR(500) NOT NULL,
  estado BOOLEAN NOT NULL,
  recomendaciones VARCHAR(500) NOT NULL,
  responsable VARCHAR(200) NOT NULL,
  idArea VARCHAR(20) NOT NULL,
  CONSTRAINT FOREIGN KEY (idArea)  REFERENCES area (idArea) ON UPDATE RESTRICT ON DELETE RESTRICT,
  dni_personal VARCHAR(8) NOT NULL,
  CONSTRAINT FOREIGN KEY (dni_personal)  REFERENCES personal (dni_personal) ON UPDATE RESTRICT ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS incidenteTecnico(
  idIncidente INTEGER PRIMARY KEY AUTO_INCREMENT ,
  nombre_incidente VARCHAR(500) NOT NULL,
  descripcion_incidente VARCHAR(500) NOT NULL,
  causa VARCHAR(500) NOT NULL,
  ocurrencias INTEGER NOT NULL,
  soluciones VARCHAR(600) NOT NULL,
  fecha_registro datetime DEFAULT NULL,
  fecha_solucion datetime DEFAULT NULL,
  idMantenimiento INTEGER NOT NULL,
  CONSTRAINT FOREIGN KEY (idMantenimiento)  REFERENCES mantenimiento (idMantenimiento) ON UPDATE RESTRICT ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS patrimonio(
  idPatrimonio INTEGER PRIMARY KEY AUTO_INCREMENT ,
  nombre_patrimonio VARCHAR(500) NOT NULL,
  descripcion_patrimonio VARCHAR(500) NOT NULL,
  prioridad VARCHAR(200) NOT NULL,
  fecha_ingreso datetime DEFAULT NULL,
  fecha_compra datetime DEFAULT NULL,
  fecha_caducidad datetime DEFAULT NULL,
  catPatrimonio VARCHAR(600) NOT NULL,
  factorRiesgo VARCHAR(600) NOT NULL,
  vulneravilidad VARCHAR(600) NOT NULL,
  tipo_patrimonio VARCHAR(600) NOT NULL,
  dni_personal VARCHAR(8) NOT NULL,
  CONSTRAINT FOREIGN KEY (dni_personal)  REFERENCES personal (dni_personal) ON UPDATE RESTRICT ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS activo(
  idActivo INTEGER PRIMARY KEY AUTO_INCREMENT ,
  caracteristicas VARCHAR(500) NOT NULL,
  fecha_ingreso datetime DEFAULT NULL,
  fecha_compra datetime DEFAULT NULL,
  categoria VARCHAR(500) NOT NULL,
  prioridad VARCHAR(200) NOT NULL,
  idPatrimonio INTEGER NOT NULL,
  tipo_activo VARCHAR(600) NOT NULL,
  CONSTRAINT FOREIGN KEY (idPatrimonio)  REFERENCES patrimonio (idPatrimonio) ON UPDATE RESTRICT ON DELETE RESTRICT
);

/*INSERT DATA FOR PROVEEDOR*/
INSERT INTO proveedor (idProveedor, primer_apellido_proveedor, segundo_apellido_proveedor, nombres_proveedor, direccion_proveedor, ruc_proveedor) VALUES 
('P001', 'Malaquias', 'Wilfredo', 'Octavio', 'Desconocido', '1012345678'),
('P002', 'Pator', 'Silva', 'Nik', 'Desconocido', '1080238972'),
('P003', 'Vasquez', 'Salinas', 'Victor', 'Desconocido', '1041765644'),
('P004', 'Cotrina', 'Chacon', 'Genesis', 'Desconocido', '1023960745');
/*INSERT DATA FOR CLIENTE*/
INSERT INTO cliente (primer_apellido_cliente, segundo_apellido_cliente, nombres_cliente, dni_cliente) VALUES
('Gutierrez', 'Medina', 'Gladys','80591498'),
('Baes', 'Medina', 'Josdy','72453858'),
('Esquivel', 'escobedo', 'Narumy','21527438');
/*INSERT DATA FOR AREA*/
INSERT INTO area (idArea, descripcion_area) VALUES 
('A001','Finanzas'),('A003','Recursos Humanos'),('A002','Contabilidad');
/*INSERT DATA FOR CARGO*/
INSERT INTO cargo (idCargo, descripcion_cargo) VALUES 
('C001','Jefe'),('C003','Secretaria'),('C002','Personal Administrativo');
/* INSERT DATA FOR PERSONAL*/
INSERT INTO personal (primer_apellido_personal, segundo_apellido_personal, nombres_personal, dni_personal, idArea, idCargo) VALUES
('Vasquez', 'Castañeda', 'Luis Angel', '44759249','A003','C003'),
('Borja', 'Reina', 'Whiston kendrick', '44939310','A001','C001'),
('Albino', 'Huertas', 'Eder Alberto', '73036543','A002','C002');

/*INSERT DATA FOR USUARIOS*/
INSERT INTO usuario (codUsuario, user_name, user_password, user_type, dni_personal, fechaCaduca) VALUES
(1, 'luis@localhost', '$2a$08$mBSNCTY7XSS.sVZc6Jrtuu5ElRFFh5hGVfzG9jPEdDC60ZymaYa0m', 'Personal', '44759249', '2017-06-14 00:00:00'),
(2, 'whiston@localhost', '$2y$10$YZAbe574aGtGMErYWzD7rulJn8/Btep23DtCu1KG5GZZZJb.F.uwu', 'Personal', '44939310', '2017-07-01 11:11:59');
/*INSERT DATA FOR ACTAS_CONFORMIDAD*/
INSERT INTO actaconformidadservicio (
  nro_identificacion,
  concepto,
  nro_contrato,
  nro_os,
  nro_doc_ref,
  fecha_conformidad,
  dni_personal,
  dni_cliente,
  idProveedor,
  estado,
  descripcion_acta,
  monto_registro_conformidad,
  monto_con_conformidad,
  monto_saldo,
  descripcion_servicio) VALUES
  (1,
  'Servicio de mantenimiento de CPU -(RSPS)',
  NULL,
  18,
  'P.S N°015 Area de estadistica e Informatica',
  '2017-07-01 11:11:59',
   '44759249',
   '80591498',
    'P001',
    1,
    'Mantenimiento, revision y reparacion del CPU de la unid. de Recursos Humanos',
   260.000,
    0000,
    0000,
    'Mantenimiento correctivo de unidad central'
  ),
  (2,
  'Servicio de mantenimiento de CPU -(RSPS)',
  NULL,
  18,
  'P.S N°015 Area de estadistica e Informatica',
  '2017-07-01 11:11:59',
   '44759249',
   '80591498',
    'P001',
    1,
    'Mantenimiento, revision y reparacion del CPU de la unid. de Recursos Humanos',
   260.000,
    0000,
    0000,
    'Mantenimiento correctivo de unidad central'
  ),
  (3,
  'Servicio de mantenimiento de CPU -(RSPS)',
  NULL,
  18,
  'P.S N°015 Area de estadistica e Informatica',
  '2017-07-01 11:11:59',
   '44759249',
   '80591498',
    'P001',
    1,
    'Mantenimiento, revision y reparacion del CPU de la unid. de Recursos Humanos',
   260.000,
    0000,
    0000,
    'Mantenimiento correctivo de unidad central'
  );
/*INSERT DATA FOR BIEN*/
INSERT INTO bien(idBien, descripcion_bien, tipoTangintang) VALUES 
(1,"computadora","bla bla bla y mas bla bla"),
(2,"Silla","bla bla bla y mas bla bla"),
(3,"Impresora","bla bla bla y mas bla bla");
/*INSERT DATA FOR UNIDAD_MEDIDA*/
INSERT INTO unidad( idUnidad, descripcion_unidad) VALUES (1,"kg"),(2,"cm"),(3,"g"),(4,"lt");
/*INSERT DATA FOR REQUERIMIENTO_BIENES*/
INSERT INTO requerimientobienes(idRequerimiento, fecha,numero_solicitud, dni_personal) VALUES 
(1,'2017-06-14','S001','44939310'),
(2,'2017-06-14','S002','44939310'),
(3,'2017-06-14','S003', '44939310');
/*INSERT DATA FOR DETALLE_REQUERIMIENTO*/
INSERT INTO detalleRequerimiento(idDetalle, cantidad, precio, idRequerimiento, idBien, idUnidad) VALUES 
(1,4,6,2,1,1),
(2,4,6,2,2,2),
(3,4,6,2,3,3);
/*INSERT DATA FOR ACTA_PEDIDO*/
INSERT INTO actapedido ( idActa, fecha, motivo, tipo_uso, idArea) VALUES
(1,'2017-06-14 00:00:00','Solicito Material de restauracion','mantenimiento', 'A001'),
(2,'2017-06-14 00:00:00','Solicito Impresorar multifuncioales','equipo', 'A002'),
(3,'2017-06-14 00:00:00','Solicito equipos de computo','equipo', 'A003');
/*INSERT DATA FOR DETALLE_PEDIDO*/
INSERT INTO detallePedido ( idDetalle, descripcion_detallePedido, clasificador, monto, idUnidad, idActa ) VALUES 
(1,'bla bla 1','urgentisisisissimo ya!!!',100.00, 1,1),
(2,'bla bla 2','urgentisisisissimo ya!!!',100.00, 2,2),
(3,'bla bla 3','urgentisisisissimo ya!!!',100.00, 3,3);
/*INSERT DATA FOR TAREA*/
INSERT INTO tarea ( descripcion_tarea, idTarea, idActa) VALUES 
('Tarea 1',1,1),
('Tarea 2',2,1),
('Tarea 3',3,2),
('Tarea 4',4,3);
/*INSERT DATA FOR MANTEMIENTO*/
INSERT INTO mantenimiento (idMantenimiento,fecha,servicio,descripcion_mantenimiento,estado,recomendaciones,responsable,idArea,dni_personal) VALUES
(1,'2017-06-14 00:00:00', 'servicio 1', 'mas bla bla',TRUE,'bla bla ','pascualillo','A001','44759249'),
(2,'2017-06-14 00:00:00', 'servicio 2', 'mas bla bla',TRUE,'bla bla ','pascualillo','A002','44939310'),
(3,'2017-06-14 00:00:00', 'servicio 3', 'mas bla bla',TRUE,'bla bla ','pascualillo','A003','44759249'),
(4,'2017-06-14 00:00:00', 'servicio 4', 'mas bla bla',TRUE,'bla bla ','pascualillo','A001','44939310');
/*INSERT DATA FOR PATRIMONIO*/
INSERT INTO incidenteTecnico ( idIncidente, nombre_incidente, descripcion_incidente, causa, ocurrencias, soluciones, fecha_registro, fecha_solucion, idMantenimiento) VALUES
(1,'incidente 1', 'ha ocurrido un incidente','no se sabe', 100, 'no se sabe', '2017-06-14 00:00:00','2017-06-14 00:00:00',1), 
(2,'incidente 2', 'ha ocurrido un incidente','no se sabe', 100, 'no se sabe', '2017-06-14 00:00:00','2017-06-14 00:00:00',2), 
(3,'incidente 3', 'ha ocurrido un incidente','no se sabe', 100, 'no se sabe', '2017-06-14 00:00:00','2017-06-14 00:00:00',3), 
(4,'incidente 4', 'ha ocurrido un incidente','no se sabe', 100, 'no se sabe', '2017-06-14 00:00:00','2017-06-14 00:00:00',4);

/*INSERT DATA FOR PATRIMONIO*/
INSERT INTO patrimonio( idPatrimonio, nombre_patrimonio, descripcion_patrimonio, prioridad, fecha_ingreso, fecha_compra, fecha_caducidad, catPatrimonio, factorRiesgo, vulneravilidad, tipo_patrimonio, dni_personal) VALUES
(1,'patrimonio 1','bueno',1,'2017-06-14 00:00:00','2017-06-14 00:00:00','2017-06-14 00:00:00',"bla bla", '0%','0%','equipo de computo','44759249'),
(2,'patrimonio 2','bueno',1,'2017-06-14 00:00:00','2017-06-14 00:00:00','2017-06-14 00:00:00',"bla bla", '0%','0%','equipo de computo','44759249'),
(3,'patrimonio 3','bueno',1,'2017-06-14 00:00:00','2017-06-14 00:00:00','2017-06-14 00:00:00',"bla bla", '0%','0%','equipo de computo','44759249'),
(4,'patrimonio 4','bueno',1,'2017-06-14 00:00:00','2017-06-14 00:00:00','2017-06-14 00:00:00',"bla bla", '0%','0%','equipo de computo','44759249');
/*INSERT DATA FOR ACTIVO*/
INSERT INTO activo (idActivo,caracteristicas,fecha_ingreso,fecha_compra,categoria,prioridad,idPatrimonio,tipo_activo) VALUES
(1,'caracteristicas 1', 'decripcion del activo','2017-06-14 00:00:00','2017-06-14 00:00:00','ninguno',1,'ninguno'),
(2,'caracteristicas 2', 'decripcion del activo','2017-06-14 00:00:00','2017-06-14 00:00:00','ninguno',1,'ninguno'),
(3,'caracteristicas 3', 'decripcion del activo','2017-06-14 00:00:00','2017-06-14 00:00:00','ninguno',1,'ninguno'),
(4,'caracteristicas 4', 'decripcion del activo','2017-06-14 00:00:00','2017-06-14 00:00:00','ninguno',1,'ninguno');