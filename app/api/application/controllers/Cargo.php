<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Cargo extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('cargo_model');
	}

	public function index_get($cargoId = NULL){
        if(! is_null($cargoId)){
            $cargo = $this->cargo_model->get($cargoId);
            $this->response($cargo,200);
        }else{
            $cargo  =$this->cargo_model->get();
            if(! is_null($cargo)){
                $this->response($cargo, 200);
            }else{
                $this->response(array("message"=>"No hay Cargos"), 200);
            }
        }
	}
	public function index_post(){
        $cargo=array(
            "idCargo"=>$this->post("idCargo"),
            "descripcion_cargo"=>$this->post("descripcion_cargo")
        );
        $cargoId= $this->cargo_model->save($cargo);
        if($cargoId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Cargo agregado exitosamente"),200);
        }else if(!$cargoId){
            $this->response(array("message"=>"Este Cargo ya existe"),400);
        }else if(is_null($cargoId)){
            $this->response(array("message"=>"Hubo un problema al guardar este Cargo"),402);
        }
	}
	public function index_put($cargoId){
        $cargo=array(
            "idCargo"=>$this->put("idCargo"),
            "descripcion_cargo"=>$this->put("descripcion_cargo")
        );
        $update= $this->cargo_model->update($cargoId, $cargo);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Cargo actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($cargoId){
        if(! $cargoId){
            $this->response(NULL, 400);
        }
        $delete = $this->cargo_model->delete($cargoId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Cargo eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}