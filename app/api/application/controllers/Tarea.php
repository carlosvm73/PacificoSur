<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Tarea extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('tarea_model');
	}

	public function index_get($tareaId = NULL){
        if(! is_null($tareaId)){
            $tarea = $this->tarea_model->get($tareaId);
            $this->response($tarea,200);
        }else{
            $tarea  =$this->tarea_model->get();
            if(! is_null($tarea)){
                $this->response($tarea, 200);
            }else{
                $this->response(array("message"=>"No hay tareas"), 200);
            }
        }
	}
	public function index_post(){
        $tarea=array(
            "descripcion_tarea"=>$this->post("descripcion_tarea"),
            "idActa"=>$this->post("idActa")
        );
        $tareaId= $this->tarea_model->save($tarea);
        if($tareaId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Tarea agregado exitosamente"),200);
        }else if(!$tareaId){
            $this->response(array("message"=>"Este tarea ya existe"),400);
        }else if(is_null($tareaId)){
            $this->response(array("message"=>"Hubo un problema al guardar este tarea"),402);
        }
	}
	public function index_put($tareaId){
        $tarea=array(
            "descripcion_tarea"=>$this->put("descripcion_tarea"),
            "idActa"=>$this->put("idActa")
        );
        $update= $this->tarea_model->update($tareaId, $tarea);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Tarea actualizada"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($tareaId){
        if(! $tareaId){
            $this->response(NULL, 400);
        }
        $delete = $this->tarea_model->delete($tareaId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Tarea eliminada"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}