<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Detallereque extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('detallereque_model');
	}

	public function index_get($detallerequeId = NULL){
        if(! is_null($detallerequeId)){
            $detallereque = $this->detallereque_model->get($detallerequeId);
            $this->response($detallereque,200);
        }else{
            $detallereque  =$this->detallereque_model->get();
            if(! is_null($detallereque)){
                $this->response($detallereque, 200);
            }else{
                $this->response(array("message"=>"No hay Detalles de requerimientos"), 200);
            }
        }
	}
	public function index_post(){
        $detallereque=array(
            "cantidad"=>$this->post("cantidad"),
            "precio"=>$this->post("precio"),
            "idRequerimiento"=>$this->post("idRequerimiento"),
            "idBien"=>$this->post("idBien"),
            "idUnidad"=>$this->post("idUnidad")
        );
        $detallerequeId= $this->detallereque_model->save($detallereque);
        if($detallerequeId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Detalles de Requerimientos agregado exitosamente"),200);
        }else if(!$detallerequeId){
            $this->response(array("message"=>"Este Detalle de requerimiento ya existe"),400);
        }else if(is_null($detallerequeId)){
            $this->response(array("message"=>"Hubo un problema al guardar este Detalle de requerimiento"),402);
        }
	}
	public function index_put($detallerequeId){
        $detallereque=array(
            "cantidad"=>$this->put("cantidad"),
            "precio"=>$this->put("precio"),
            "idRequerimiento"=>$this->put("idRequerimiento"),
            "idBien"=>$this->put("idBien"),
            "idUnidad"=>$this->put("idUnidad")
        );
        $update= $this->detallereque_model->update($detallerequeId, $detallereque);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Detalle de requerimiento actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($detallerequeId){
        if(! $detallerequeId){
            $this->response(NULL, 400);
        }
        $delete = $this->detallereque_model->delete($detallerequeId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Detalle de requerimiento eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}