<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Unidad extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('unidad_model');
	}

	public function index_get($unidadId = NULL){
        if(! is_null($unidadId)){
            $unidad = $this->unidad_model->get($unidadId);
            $this->response($unidad,200);
        }else{
            $unidad  =$this->unidad_model->get();
            if(! is_null($unidad)){
                $this->response($unidad, 200);
            }else{
                $this->response(array("message"=>"No hay Unidades"), 200);
            }
        }
	}
	public function index_post(){
        $unidad=array(
            "descripcion_unidad"=>$this->post("descripcion_unidad")
        );
        $unidadId= $this->unidad_model->save($unidad);
        if($unidadId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Unidad agregado exitosamente"),200);
        }else if(!$unidadId){
            $this->response(array("message"=>"Este Unidad ya existe"),400);
        }else if(is_null($unidadId)){
            $this->response(array("message"=>"Hubo un problema al guardar esta Unidad"),402);
        }
	}
	public function index_put($unidadId){
        $unidad=array(
            "descripcion_unidad"=>$this->put("descripcion_unidad")
        );
        $update= $this->unidad_model->update($unidadId, $unidad);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Unidad actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($unidadId){
        if(! $unidadId){
            $this->response(NULL, 400);
        }
        $delete = $this->unidad_model->delete($unidadId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Unidad eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}