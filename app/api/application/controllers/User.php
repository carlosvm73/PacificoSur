<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class User extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
	}

	public function index_get($userId = NULL){
        if(! is_null($userId)){
            $user = $this->user_model->get($userId);
            $this->response($user,200);
        }else{
            $user  =$this->user_model->get();
            if(! is_null($user)){
                $this->response($user, 200);
            }else{
                $this->response(array("message"=>"No hay usuarios"), 200);
            }
        }
	}

    public function index_post(){
        $user = array("user_name"=>$this->post('user_name'), 
                    "user_password"=>password_hash($this->post('user_password'), PASSWORD_BCRYPT),
                    "user_type"=>$this->post('user_type'),
                    "dni_personal"=>$this->post('dni_personal'));
        $UserId= $this->user_model->save($user);
        if($UserId){
            $this->response(array("data"=>$this->index_get(), "message"=>"El Usuario ha sido registrado"),200);
        }else if(!$UserId){
            $this->response(array("message"=>"Este Usuario ya existe"),400);
        }else if(is_null($UserId)){
            $this->response(array("message"=>"Hubo un problema al guardar este usuario"),500);
        }
    }
	public function index_put($userId){
        if(strlen($this->put('user_password')) > 0){
            $user=array(
                "user_name" => $this->put('user_name'),
                "user_password" => password_hash($this->put('user_password'), PASSWORD_BCRYPT),
                "user_type" => $this->put('user_type'),
                "dni_personal" => $this->put('dni_personal')
            );
        }else{
            $user=array(
                "user_name" => $this->put('user_name'),
                "user_type" => $this->put('user_type'),
                "dni_personal" => $this->put('dni_personal')
            );
        }
        $update= $this->user_model->update($userId, $user);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Cuenta actualizada"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($userId){
        if(! $userId){
            $this->response(NULL, 400);
        }
        $delete = $this->user_model->delete($userId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Personal eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}