<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Incidente extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('incidente_model');
	}

	public function index_get($incidenteId = NULL){
        if(! is_null($incidenteId)){
            $incidente = $this->incidente_model->get($incidenteId);
            $this->response($incidente,200);
        }else{
            $incidente  =$this->incidente_model->get();
            if(! is_null($incidente)){
                $this->response($incidente, 200);
            }else{
                $this->response(array("message"=>"No hay incidente"), 200);
            }
        }
	}
	public function index_post(){
        $incidente=array(
            "nombre_incidente"=>$this->post("nombre_incidente"),
            "descripcion_incidente"=>$this->post("descripcion_incidente"),
            "causa"=>$this->post("causa"),
            "ocurrencias"=>$this->post("ocurrencias"),
            "soluciones"=>$this->post("soluciones"),
            "fecha_registro"=>$this->post("fecha_registro"),
            "fecha_solucion"=>$this->post("fecha_solucion"),
            "idMantenimiento"=>$this->post("idMantenimiento")
        );
        $incidenteId= $this->incidente_model->save($incidente);
        if($incidenteId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Incidente agregado exitosamente"),200);
        }else if(!$incidenteId){
            $this->response(array("message"=>"Este incidente ya existe"),400);
        }else if(is_null($incidenteId)){
            $this->response(array("message"=>"Hubo un problema al guardar este incidente"),402);
        }
	}
	public function index_put($incidenteId){
        $incidente=array(
            "nombre_incidente"=>$this->put("nombre_incidente"),
            "descripcion_incidente"=>$this->put("descripcion_incidente"),
            "causa"=>$this->put("causa"),
            "ocurrencias"=>$this->put("ocurrencias"),
            "soluciones"=>$this->put("soluciones"),
            "fecha_registro"=>$this->put("fecha_registro"),
            "fecha_solucion"=>$this->put("fecha_solucion"),
            "idMantenimiento"=>$this->put("idMantenimiento")
        );
        $update= $this->incidente_model->update($incidenteId, $incidente);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"incidente actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($incidenteId){
        if(! $incidenteId){
            $this->response(NULL, 400);
        }
        $delete = $this->incidente_model->delete($incidenteId);
        if($delete){
            $this->response(array("data"=>$this->index_get(),"message"=>"incidente eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}