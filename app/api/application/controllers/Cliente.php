<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Cliente extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('cliente_model');
	}

	public function index_get($clienteId = NULL){
        if(! is_null($clienteId)){
            $cliente = $this->cliente_model->get($clienteId);
            $this->response($cliente,200);
        }else{
            $cliente  =$this->cliente_model->get();
            if(! is_null($cliente)){
                $this->response($cliente, 200);
            }else{
                $this->response(array("message"=>"No hay Clientes"), 200);
            }
        }
	}
	public function index_post(){
        $cliente=array(
            "primer_apellido_cliente"=>$this->post("primer_apellido_cliente"),
            "segundo_apellido_cliente"=>$this->post("segundo_apellido_cliente"),
            "nombres_cliente"=>$this->post("nombres_cliente"),
            "dni_cliente"=>$this->post("dni_cliente")
        );
        $clienteId= $this->cliente_model->save($cliente);
        if($clienteId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Cliente agregado exitosamente"),200);
        }else if(!$clienteId){
            $this->response(array("message"=>"Este Cliente ya existe"),400);
        }else if(is_null($clienteId)){
            $this->response(array("message"=>"Hubo un problema al guardar este Cliente"),402);
        }
	}
	public function index_put($clienteId){
        $cliente=array(
            "primer_apellido_cliente"=>$this->put("primer_apellido_cliente"),
            "segundo_apellido_cliente"=>$this->put("segundo_apellido_cliente"),
            "nombres_cliente"=>$this->put("nombres_cliente"),
            "dni_cliente"=>$this->put("dni_cliente")
        );
        $update= $this->cliente_model->update($clienteId, $cliente);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Cliente actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($clienteId){
        if(! $clienteId){
            $this->response(NULL, 400);
        }
        $delete = $this->cliente_model->delete($clienteId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Cliente eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}