<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Patri extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('patrimonio_model');
	}

	public function index_get($patrimonioId = NULL){
        if(! is_null($patrimonioId)){
            $patrimonio = $this->patrimonio_model->get($patrimonioId);
            $this->response($patrimonio,200);
        }else{
            $patrimonio  =$this->patrimonio_model->get();
            if(! is_null($patrimonio)){
                $this->response($patrimonio, 200);
            }else{
                $this->response(array("message"=>"No hay Patrimonio"), 200);
            }
        }
	}
	public function index_post(){
        $patrimonio=array(
            "nombre_patrimonio"=>$this->post("nombre_patrimonio"),
            "descripcion_patrimonio"=>$this->post("descripcion_patrimonio"),
            "prioridad"=>$this->post("prioridad"),
            "fecha_ingreso"=>$this->post("fecha_ingreso"),
            "fecha_compra"=>$this->post("fecha_compra"),
            "fecha_caducidad"=>$this->post("fecha_caducidad"),
            "catPatrimonio"=>$this->post("catPatrimonio"),
            "factorRiesgo"=>$this->post("factorRiesgo"),
            "vulneravilidad"=>$this->post("vulneravilidad"),
            "tipo_patrimonio"=>$this->post("tipo_patrimonio"),
            "dni_personal"=>$this->post("dni_personal")
        );
        $patrimonioId= $this->patrimonio_model->save($patrimonio);
        if($patrimonioId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Pesonal agregado exitosamente"),200);
        }else if(!$patrimonioId){
            $this->response(array("message"=>"Este Patrimonio ya existe"),400);
        }else if(is_null($patrimonioId)){
            $this->response(array("message"=>"Hubo un problema al guardar este Patrimonio"),402);
        }
	}
	public function index_put($patrimonioId){
        $patrimonio=array(
            "nombre_patrimonio"=>$this->put("nombre_patrimonio"),
            "descripcion_patrimonio"=>$this->put("descripcion_patrimonio"),
            "prioridad"=>$this->put("prioridad"),
            "fecha_ingreso"=>$this->put("fecha_ingreso"),
            "fecha_compra"=>$this->put("fecha_compra"),
            "fecha_caducidad"=>$this->put("fecha_caducidad"),
            "catPatrimonio"=>$this->put("catPatrimonio"),
            "factorRiesgo"=>$this->put("factorRiesgo"),
            "vulneravilidad"=>$this->put("vulneravilidad"),
            "tipo_patrimonio"=>$this->put("tipo_patrimonio"),
            "dni_personal"=>$this->put("dni_personal")
        );
        $update= $this->patrimonio_model->update($patrimonioId, $patrimonio);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Patrimonio actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($patrimonioId){
        if(! $patrimonioId){
            $this->response(NULL, 400);
        }
        $delete = $this->patrimonio_model->delete($patrimonioId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Patrimonio eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}