<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Proveedor extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('proveedor_model');
	}

	public function index_get($proveedorId = NULL){
        if(! is_null($proveedorId)){
            $roveedores = $this->proveedor_model->get($proveedorId);
            $this->response($roveedores,200);
        }else{
            $roveedores  =$this->proveedor_model->get();
            if(! is_null($roveedores)){
                $this->response($roveedores, 200);
            }else{
                $this->response(array("message"=>"No hay proveedores"), 200);
            }
        }
	}
	public function index_post(){
        $roveedores=array(
            "idProveedor"=>$this->post("idProveedor"),
            "primer_apellido_proveedor"=>$this->post("primer_apellido_proveedor"),
            "segundo_apellido_proveedor"=>$this->post("segundo_apellido_proveedor"),
            "nombres_proveedor"=>$this->post("nombres_proveedor"),
            "direccion_proveedor"=>$this->post("direccion_proveedor"),
            "ruc_proveedor"=>$this->post("ruc_proveedor")
        );
        $proveedorId= $this->proveedor_model->save($roveedores);
        if($proveedorId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Proveedor agregado exitosamente"),200);
        }else if(!$proveedorId){
            $this->response(array("message"=>"Este Proveedor ya existe"),400);
        }else if(is_null($proveedorId)){
            $this->response(array("message"=>"Hubo un problema al guardar este Proveedor"),402);
        }
	}
	public function index_put($proveedorId){
        $roveedores=array(
            "idProveedor"=>$this->put("idProveedor"),
            "primer_apellido_proveedor"=>$this->put("primer_apellido_proveedor"),
            "segundo_apellido_proveedor"=>$this->put("segundo_apellido_proveedor"),
            "nombres_proveedor"=>$this->put("nombres_proveedor"),
            "direccion_proveedor"=>$this->put("direccion_proveedor"),
            "ruc_proveedor"=>$this->put("ruc_proveedor")
        );
        $update= $this->proveedor_model->update($proveedorId, $roveedores);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Proveedor actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($proveedorId){
        if(! $proveedorId){
            $this->response(NULL, 400);
        }
        $delete = $this->proveedor_model->delete($proveedorId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Proveedor eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}