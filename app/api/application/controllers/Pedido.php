<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Pedido extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('pedido_model');
	}

	public function index_get($pedidoId = NULL){
        if(! is_null($pedidoId)){
            $pedido = $this->pedido_model->get($pedidoId);
            $this->response($pedido,200);
        }else{
            $pedido  =$this->pedido_model->get();
            if(! is_null($pedido)){
                $this->response($pedido, 200);
            }else{
                $this->response(array("message"=>"No hay Pedidos"), 200);
            }
        }
	}
	public function index_post(){
        $pedido=array(
            "fecha"=>$this->post("fecha"),
            "motivo"=>$this->post("motivo"),
            "tipo_uso"=>$this->post("tipo_uso"),
            "idArea"=>$this->post("idArea")
        );
        $pedidoId= $this->pedido_model->save($pedido);
        if($pedidoId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Pedido agregado exitosamente"),200);
        }else if(!$pedidoId){
            $this->response(array("message"=>"Este Pedido ya existe"),400);
        }else if(is_null($pedidoId)){
            $this->response(array("message"=>"Hubo un problema al guardar este Pedido"),402);
        }
	}
	public function index_put($pedidoId){
        $pedido=array(
            "fecha"=>$this->put("fecha"),
            "motivo"=>$this->put("motivo"),
            "tipo_uso"=>$this->put("tipo_uso"),
            "idArea"=>$this->put("idArea")
        );
        $update= $this->pedido_model->update($pedidoId, $pedido);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Pedido actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($pedidoId){
        if(! $pedidoId){
            $this->response(NULL, 400);
        }
        $delete = $this->pedido_model->delete($pedidoId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Pedido eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}