<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Bien extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('bien_model');
	}

	public function index_get($bienId = NULL){
        if(! is_null($bienId)){
            $bien = $this->bien_model->get($bienId);
            $this->response($bien,200);
        }else{
            $bien  =$this->bien_model->get();
            if(! is_null($bien)){
                $this->response($bien, 200);
            }else{
                $this->response(array("message"=>"No hay Bienes"), 200);
            }
        }
	}
	public function index_post(){
        $bien=array(
            "descripcion_bien"=>$this->post("descripcion_bien"),
            "tipoTangintang"=>$this->post("tipoTangintang")
        );
        $bienId= $this->bien_model->save($bien);
        if($bienId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Bien agregado exitosamente"),200);
        }else if(!$bienId){
            $this->response(array("message"=>"Este Bien ya existe"),400);
        }else if(is_null($bienId)){
            $this->response(array("message"=>"Hubo un problema al guardar este Bien"),402);
        }
	}
	public function index_put($bienId){
        $bien=array(
            "descripcion_bien"=>$this->put("descripcion_bien"),
            "tipoTangintang"=>$this->put("tipoTangintang")
        );
        $update= $this->bien_model->update($bienId, $bien);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Bien actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($bienId){
        if(! $bienId){
            $this->response(NULL, 400);
        }
        $delete = $this->bien_model->delete($bienId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Bien eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}