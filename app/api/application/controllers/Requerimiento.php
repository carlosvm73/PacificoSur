<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Requerimiento extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('requerimiento_model');
	}

	public function index_get($requerimientoId = NULL){
        if(! is_null($requerimientoId)){
            $requerimiento = $this->requerimiento_model->get($requerimientoId);
            $this->response($requerimiento,200);
        }else{
            $requerimiento  =$this->requerimiento_model->get();
            if(! is_null($requerimiento)){
                $this->response($requerimiento, 200);
            }else{
                $this->response(array("message"=>"No hay requerimientos"), 200);
            }
        }
	}
	public function index_post(){
        $requerimiento=array(
                "fecha"=>$this->post("fecha"),
                "numero_solicitud"=>$this->post("numero_solicitud"),
                "dni_personal"=>$this->post("dni_personal")
        );
        $requerimientoId= $this->requerimiento_model->save($requerimiento);
        if($requerimientoId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Requerimiento agregado exitosamente"),200);
        }else if(!$requerimientoId){
            $this->response(array("message"=>"Este requerimiento ya existe"),400);
        }else if(is_null($requerimientoId)){
            $this->response(array("message"=>"Hubo un problema al guardar este requerimiento"),402);
        }
	}
	public function index_put($requerimientoId){
        $requerimiento=array(
                "fecha"=>$this->put("fecha"),
                "numero_solicitud"=>$this->put("numero_solicitud"),
                "dni_personal"=>$this->put("dni_personal")
        );
        $update= $this->requerimiento_model->update($requerimientoId, $requerimiento);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Requerimiento actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($requerimientoId){
        if(! $requerimientoId){
            $this->response(NULL, 400);
        }
        $delete = $this->requerimiento_model->delete($requerimientoId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Requerimiento eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}