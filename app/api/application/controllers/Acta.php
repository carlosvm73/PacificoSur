<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Acta extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('acta_model');
	}

	public function index_get($actaId = NULL){
        if(! is_null($actaId)){
            $actaea = $this->acta_model->get($actaId);
            $this->response($actaea,200);
        }else{
            $actaea  =$this->acta_model->get();
            if(! is_null($actaea)){
                $this->response($actaea, 200);
            }else{
                $this->response(array("message"=>"No hay Actas"), 200);
            }
        }
	}
	public function index_post(){
        $actaea=array(
            "concepto"=>$this->post("concepto"),
            "nro_contrato"=>$this->post("nro_contrato"),
            "nro_os"=>$this->post("nro_os"),
            "nro_doc_ref"=>$this->post("nro_doc_ref"),
            "fecha_conformidad"=>$this->post("fecha_conformidad"),
            "dni_personal"=>$this->post("dni_personal"),
            "dni_cliente"=>$this->post("dni_cliente"),
            "idProveedor"=>$this->post("idProveedor"),
            "estado"=>$this->post("estado"),
            "descripcion_acta"=>$this->post("descripcion_acta"),
            "monto_registro_conformidad"=>$this->post("monto_registro_conformidad"),
            "monto_con_conformidad"=>$this->post("monto_con_conformidad"),
            "monto_saldo"=>$this->post("monto_saldo"),
            "descripcion_servicio"=>$this->post("descripcion_servicio")
        );
        $actaId= $this->acta_model->save($actaea);
        if($actaId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Acta agregado exitosamente"),200);
        }else if(!$actaId){
            $this->response(array("message"=>"Este Acta ya existe"),400);
        }else if(is_null($actaId)){
            $this->response(array("message"=>"Hubo un problema al guardar este Acta"),402);
        }
	}
	public function index_put($actaId){
        $actaea=array(
            "nro_identificacion"=>$this->put("nro_identificacion"),
            "concepto"=>$this->put("concepto"),
            "nro_contrato"=>$this->put("nro_contrato"),
            "nro_os"=>$this->put("nro_os"),
            "nro_doc_ref"=>$this->put("nro_doc_ref"),
            "fecha_conformidad"=>$this->put("fecha_conformidad"),
            "dni_personal"=>$this->put("dni_personal"),
            "dni_cliente"=>$this->put("dni_cliente"),
            "idProveedor"=>$this->put("idProveedor"),
            "estado"=>$this->put("estado"),
            "descripcion_acta"=>$this->put("descripcion_acta"),
            "monto_registro_conformidad"=>$this->put("monto_registro_conformidad"),
            "monto_con_conformidad"=>$this->put("monto_con_conformidad"),
            "monto_saldo"=>$this->put("monto_saldo"),
            "descripcion_servicio"=>$this->put("descripcion_servicio")
        );
        $update= $this->acta_model->update($actaId, $actaea);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Acta actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($actaId){
        if(! $actaId){
            $this->response(NULL, 400);
        }
        $delete = $this->acta_model->delete($ActaId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Acta eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}