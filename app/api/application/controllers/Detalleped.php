<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Detalleped extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('detalleped_model');
	}

	public function index_get($detallerequeId = NULL){
        if(! is_null($detallerequeId)){
            $detallereque = $this->detalleped_model->get($detallerequeId);
            $this->response($detallereque,200);
        }else{
            $detallereque  =$this->detalleped_model->get();
            if(! is_null($detallereque)){
                $this->response($detallereque, 200);
            }else{
                $this->response(array("message"=>"No hay Detalles de detalle de pedidos"), 200);
            }
        }
	}
	public function index_post(){
        $detallereque=array(
            "descripcion_detallePedido"=>$this->post("descripcion_detallePedido"),
            "clasificador"=>$this->post("clasificador"),
            "monto"=>$this->post("monto"),
            "idUnidad"=>$this->post("idUnidad"),
            "idActa"=>$this->post("idActa")
        );
        $detallerequeId= $this->detalleped_model->save($detallereque);
        if($detallerequeId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Detalles de detalle de pedidos agregado exitosamente"),200);
        }else if(!$detallerequeId){
            $this->response(array("message"=>"Este Detalle de detalle de pedido ya existe"),400);
        }else if(is_null($detallerequeId)){
            $this->response(array("message"=>"Hubo un problema al guardar este Detalle de detalle de pedido"),402);
        }
	}
	public function index_put($detallerequeId){
        $detallereque=array(
            "descripcion_detallePedido"=>$this->put("descripcion_detallePedido"),
            "clasificador"=>$this->put("clasificador"),
            "monto"=>$this->put("monto"),
            "idUnidad"=>$this->put("idUnidad"),
            "idActa"=>$this->put("idActa")
        );
        $update= $this->detalleped_model->update($detallerequeId, $detallereque);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Detalle de detalle de pedido actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($detallerequeId){
        if(! $detallerequeId){
            $this->response(NULL, 400);
        }
        $delete = $this->detalleped_model->delete($detallerequeId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Detalle de detalle de pedido eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}