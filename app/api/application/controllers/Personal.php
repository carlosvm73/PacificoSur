<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Personal extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('personal_model');
	}

	public function index_get($personalId = NULL){
        if(! is_null($personalId)){
            $personal = $this->personal_model->get($personalId);
            $this->response($personal,200);
        }else{
            $personal  =$this->personal_model->get();
            if(! is_null($personal)){
                $this->response($personal, 200);
            }else{
                $this->response(array("message"=>"No hay personal"), 200);
            }
        }
	}
	public function index_post(){
        $personal=array(
            "primer_apellido_personal"=>$this->post("primer_apellido_personal"),
            "segundo_apellido_personal"=>$this->post("segundo_apellido_personal"),
            "nombres_personal"=>$this->post("nombres_personal"),
            "dni_personal"=>$this->post("dni_personal"),
            "idCargo"=>$this->post("idCargo"),
            "idArea"=>$this->post("idArea")
        );
        $personalId= $this->personal_model->save($personal);
        if($personalId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Pesonal agregado exitosamente"),200);
        }else if(!$personalId){
            $this->response(array("message"=>"Este Personal ya existe"),400);
        }else if(is_null($personalId)){
            $this->response(array("message"=>"Hubo un problema al guardar este personal"),402);
        }
	}
	public function index_put($personalId){
        $personal=array(
            "primer_apellido_personal"=>$this->put("primer_apellido_personal"),
            "segundo_apellido_personal"=>$this->put("segundo_apellido_personal"),
            "nombres_personal"=>$this->put("nombres_personal"),
            "dni_personal"=>$this->put("dni_personal"),
            "idCargo"=>$this->put("idCargo"),
            "idArea"=>$this->put("idArea")
        );
        $update= $this->personal_model->update($personalId, $personal);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Personal actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($personalId){
        if(! $personalId){
            $this->response(NULL, 400);
        }
        $delete = $this->personal_model->delete($personalId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Personal eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}