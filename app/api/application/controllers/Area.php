<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Area extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('area_model');
	}

	public function index_get($areaId = NULL){
        if(! is_null($areaId)){
            $area = $this->area_model->get($areaId);
            $this->response($area,200);
        }else{
            $area  =$this->area_model->get();
            if(! is_null($area)){
                $this->response($area, 200);
            }else{
                $this->response(array("message"=>"No hay Areas"), 200);
            }
        }
	}
	public function index_post(){
        $area=array(
            "idArea"=>$this->post("idArea"),
            "descripcion_area"=>$this->post("descripcion_area")
        );
        $areaId= $this->area_model->save($area);
        if($areaId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Area agregado exitosamente"),200);
        }else if(!$areaId){
            $this->response(array("message"=>"Este Area ya existe"),400);
        }else if(is_null($areaId)){
            $this->response(array("message"=>"Hubo un problema al guardar este Area"),402);
        }
	}
	public function index_put($areaId){
        $area=array(
            "idArea"=>$this->put("idArea"),
            "descripcion_area"=>$this->put("descripcion_area")
        );
        $update= $this->area_model->update($areaId, $area);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Area actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($areaId){
        if(! $areaId){
            $this->response(NULL, 400);
        }
        $delete = $this->area_model->delete($areaId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Area eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}