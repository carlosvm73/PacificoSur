<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Activo extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('activo_model');
	}

	public function index_get($activoId = NULL){
        if(! is_null($activoId)){
            $activo = $this->activo_model->get($activoId);
            $this->response($activo,200);
        }else{
            $activo  =$this->activo_model->get();
            if(! is_null($activo)){
                $this->response($activo, 200);
            }else{
                $this->response(array("message"=>"No hay Activos"), 200);
            }
        }
	}
	public function index_post(){
        $activo=array(
            "caracteristicas"=>$this->post("caracteristicas"),
            "fecha_ingreso"=>$this->post("fecha_ingreso"),
            "fecha_compra"=>$this->post("fecha_compra"),
            "categoria"=>$this->post("categoria"),
            "prioridad"=>$this->post("prioridad"),
            "idPatrimonio"=>$this->post("idPatrimonio"),
            "tipo_activo"=>$this->post("tipo_activo")
        );
        $activoId= $this->activo_model->save($activo);
        if($activoId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Activo agregado exitosamente"),200);
        }else if(!$activoId){
            $this->response(array("message"=>"Este Activo ya existe"),400);
        }else if(is_null($activoId)){
            $this->response(array("message"=>"Hubo un problema al guardar este Activo"),402);
        }
	}
	public function index_put($activoId){
        $activo=array(
            "caracteristicas"=>$this->put("caracteristicas"),
            "fecha_ingreso"=>$this->put("fecha_ingreso"),
            "fecha_compra"=>$this->put("fecha_compra"),
            "categoria"=>$this->put("categoria"),
            "prioridad"=>$this->put("prioridad"),
            "idPatrimonio"=>$this->put("idPatrimonio"),
            "tipo_activo"=>$this->put("tipo_activo")
        );
        $update= $this->activo_model->update($activoId, $activo);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Activo actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($activoId){
        if(! $activoId){
            $this->response(NULL, 400);
        }
        $delete = $this->activo_model->delete($activoId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Activo eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}