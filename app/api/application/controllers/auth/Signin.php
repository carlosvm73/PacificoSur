<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Signin extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
	}

    public function index_post(){
        $user = array("user_name"=>$this->post('user_name'), "user_password"=>$this->post('user_password'));
        $paylod=$this->user_model->verify($user);
        if(password_verify($user["user_password"],$paylod[0]["user_password"])){

			$this->db->set(array("fechaCaduca"=>date("Y-m-d H:i:s")))->where("dni_personal",$paylod[0]["dni_personal"])->update("usuario");
        // $this->response($paylod,200);
            $this->response(
                array("data"=>$this->newReturn($paylod), 
                // array("data"=>$paylod, 
                "access"=>true,
                "token"=>$this->createToken($paylod),
                "message"=>"Ha iniciado session satisfactoriamente"
                ),200);
        }
        $this->response(array("message"=>"Las credenciales no son correctas", "access"=>false),400);
	}

    private function createToken($user){

            return $this->jwt->encode(array(
            'sub'=>$user[0]['dni_personal'],
            'iat'=>strtotime("now"),
            'exp'=>date(DATE_ISO8601, strtotime("now"))
            ), "sdsdfdlfksjdflksdjflskd");

    }

    private function decodedToken($user){

    }

      private function newReturn($user){
        return array(
            "codUsuario"=>$user[0]["codUsuario"],
            "user_name"=>$user[0]["user_name"],
            "user_type"=>$user[0]["user_type"],
            "fechaCaduca"=>$user[0]["fechaCaduca"],
            "dni_personal"=>$user[0]["dni_personal"],
            "primer_apellido_personal"=>$user[0]["primer_apellido_personal"],
            "segundo_apellido_personal"=>$user[0]["segundo_apellido_personal"],
            "nombres_personal"=>$user[0]["nombres_personal"],
            "idCargo"=>$user[0]["idCargo"],
            "idArea"=>$user[0]["idArea"],
            "descripcion_cargo"=>$user[0]["descripcion_cargo"],
            "descripcion_area"=>$user[0]["descripcion_area"]
        );
    }

}
