<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
class Mantenimiento extends REST_Controller {
	
    public function __construct(){
		parent::__construct();
		$this->load->model('mantenimiento_model');
	}

	public function index_get($mantenimientoId = NULL){
        if(! is_null($mantenimientoId)){
            $mantenimiento = $this->mantenimiento_model->get($mantenimientoId);
            $this->response($mantenimiento,200);
        }else{
            $mantenimiento  =$this->mantenimiento_model->get();
            if(! is_null($mantenimiento)){
                $this->response($mantenimiento, 200);
            }else{
                $this->response(array("message"=>"No hay Mantenimiento"), 200);
            }
        }
	}
	public function index_post(){
        $mantenimiento=array(
            "fecha"=>$this->post("fecha"),
            "servicio"=>$this->post("servicio"),
            "descripcion_mantenimiento"=>$this->post("descripcion_mantenimiento"),
            "estado"=>$this->post("estado"),
            "recomendaciones"=>$this->post("recomendaciones"),
            "responsable"=>$this->post("responsable"),
            "idArea"=>$this->post("idArea"),
            "dni_personal"=>$this->post("dni_personal")
        );
        $mantenimientoId= $this->mantenimiento_model->save($mantenimiento);
        if($mantenimientoId){
            $this->response(array("data"=>$this->index_get(), "message"=>"Pesonal agregado exitosamente"),200);
        }else if(!$mantenimientoId){
            $this->response(array("message"=>"Este Mantenimiento ya existe"),400);
        }else if(is_null($mantenimientoId)){
            $this->response(array("message"=>"Hubo un problema al guardar este Mantenimiento"),402);
        }
	}
	public function index_put($mantenimientoId){
        $mantenimiento=array(
            "fecha"=>$this->put("fecha"),
            "servicio"=>$this->put("servicio"),
            "descripcion_mantenimiento"=>$this->put("descripcion_mantenimiento"),
            "estado"=>$this->put("estado"),
            "recomendaciones"=>$this->put("recomendaciones"),
            "responsable"=>$this->put("responsable"),
            "idArea"=>$this->put("idArea"),
            "dni_personal"=>$this->put("dni_personal")
        );
        $update= $this->mantenimiento_model->update($mantenimientoId, $mantenimiento);
        if(! is_null($update)){
            $this->response(array("data"=>$this->index_get(), "message"=>"Mantenimiento actualizado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}
	public function index_delete($mantenimientoId){
        if(! $mantenimientoId){
            $this->response(NULL, 400);
        }
        $delete = $this->mantenimiento_model->delete($mantenimientoId);
        if($delete){
            $this->response(array("data"=>$this->index_get(), "message"=>"Mantenimiento eliminado"),200);
        }else{
            $this->response(array("message"=>"Ha ocurrido un error"),400);
        }

	}

}