<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Personal_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function get($personalId = NULL){
		$this->db->select('*');
		$this->db->from('personal');
		if(! is_null($personalId)){
			$this->db->where('dni_personal',$personalId);
		}
		$this->db->join('cargo', 'personal.idCargo = cargo.idCargo');
		$this->db->join('area', 'personal.idArea = area.idArea');
		// $this->db->join('usuario', 'personal.dni_personal = usuario.dni_personal');
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}
	// public function get($personalId = NULL){
	// 	if(! is_null($personalId)){
	// 		$query = $this->db->select("*")->from('personal')->where("dni_personal", $personalId)->get();
	// 		return $query->result_array();
	// 	}else{
	// 		$query = $this->db->select("*")->from('personal')->get();
	// 		if($query->num_rows()>0){
	// 			return $query->result_array();
	// 		}
	// 	}
	// }
	public function save($personal){
        $query = $this->db->select("*")->from('personal')->where("dni_personal", $personal["dni_personal"])->get();
        if($query->num_rows() != 1 ){
            $this->db->set( $personal )->insert("personal");
            if($this->db->affected_rows() === 1 ){
                return TRUE;
            }
            return NULL;
        }
        return FALSE;
	}
	public function update($personalId, $data){
		$this->db->set($data)->where("dni_personal",$personalId)->update("personal");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($personalId){
		$this->db->where("dni_personal", $personalId)->delete("personal");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}