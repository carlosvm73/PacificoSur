<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mantenimiento_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function get($actaId = NULL){
		$this->db->select('*');
		$this->db->from('mantenimiento');
		if(! is_null($actaId)){
			$this->db->where('idMantenimiento',$actaId);
		}
		$this->db->join('personal', 'mantenimiento.dni_personal = personal.dni_personal');
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}
	public function save($mantenimiento){
		$this->db->set( $mantenimiento )->insert("mantenimiento");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;
	}
	public function update($mantenimientoId, $data){
		$this->db->set($data)->where("idMantenimiento",$mantenimientoId)->update("mantenimiento");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($mantenimientoId){
		$this->db->where("idMantenimiento", $mantenimientoId)->delete("mantenimiento");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}