<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bien_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function get($bienId = NULL){
		if(! is_null($bienId)){
			$query = $this->db->select("*")->from('bien')->where("idBien", $bienId)->get();
			return $query->result_array();
		}else{
			$query = $this->db->select("*")->from('bien')->get();
			if($query->num_rows()>0){
				return $query->result_array();
			}
		}
	}
	public function save($bien){
		$this->db->set( $bien )->insert("bien");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
        return NULL;
	}
	public function update($bienId, $data){
		$this->db->set($data)->where("idBien",$bienId)->update("bien");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($bienId){
		$this->db->where("idBien", $bienId)->delete("bien");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}