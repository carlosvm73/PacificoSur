<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedor_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function get($proveedorId = NULL){
		if(! is_null($proveedorId)){
			$query = $this->db->select("*")->from('proveedor')->where("idProveedor", $proveedorId)->get();
			return $query->result_array();
		}else{
			$query = $this->db->select("*")->from('proveedor')->get();
			if($query->num_rows()>0){
				return $query->result_array();
			}
		}
	}
	public function save($proveedor){
        $query = $this->db->select("*")->from('proveedor')->where("idProveedor", $proveedor["idProveedor"])->get();
        if($query->num_rows() != 1 ){
            $this->db->set( $proveedor )->insert("proveedor");
            if($this->db->affected_rows() === 1 ){
                return TRUE;
            }
            return NULL;
        }
        return FALSE;
	}
	public function update($proveedorId, $data){
		$this->db->set($data)->where("idProveedor",$proveedorId)->update("proveedor");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($proveedorId){
		$this->db->where("idProveedor", $proveedorId)->delete("proveedor");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}