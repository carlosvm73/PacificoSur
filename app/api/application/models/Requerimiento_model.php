<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Requerimiento_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	
	public function get($actaId = NULL){
		$this->db->select('*');
		$this->db->from('requerimientobienes');
		if(! is_null($actaId)){
			$this->db->where('idRequerimiento',$actaId);
		}
		$this->db->join('personal', 'requerimientobienes.dni_personal = personal.dni_personal');
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}
	public function save($requerimiento){
        // $query = $this->db->select("*")->from('requerimientobienes')->where("idRequerimiento", $requerimiento["idRequerimiento"])->get();
        // if($query->num_rows() != 1 ){
        // }
        // return FALSE;
		$this->db->set( $requerimiento )->insert("requerimientobienes");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;
	}
	public function update($requerimientoId, $data){
		$this->db->set($data)->where("idRequerimiento",$requerimientoId)->update("requerimientobienes");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($requerimientoId){
		$this->db->where("idRequerimiento", $requerimientoId)->delete("requerimientobienes");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}