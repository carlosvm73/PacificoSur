<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tarea_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function get($tareaId = NULL){
		if(! is_null($tareaId)){
			$query = $this->db->select("*")->from('tarea')->where("idTarea", $tareaId)->get();
			return $query->result_array();
		}else{
			$query = $this->db->select("*")->from('tarea')->get();
			if($query->num_rows()>0){
				return $query->result_array();
			}
		}
	}
	public function save($tarea){
        $query = $this->db->select("*")->from('tarea')->where("idTarea", $tarea["idTarea"])->get();
        if($query->num_rows() != 1 ){
            $this->db->set( $tarea )->insert("tarea");
            if($this->db->affected_rows() === 1 ){
                return TRUE;
            }
            return NULL;
        }
        return FALSE;
	}
	public function update($tareaId, $data){
		$this->db->set($data)->where("idTarea",$tareaId)->update("tarea");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($tareaId){
		$this->db->where("idTarea", $tareaId)->delete("tarea");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}