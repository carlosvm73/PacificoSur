<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pedido_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function get($actapedidoId = NULL){
		$this->db->select('*');
		$this->db->from('actapedido');
		if(! is_null($actapedidoId)){
			$this->db->where('idActa',$actapedidoId);
		}
		$this->db->join('area', 'actapedido.idArea = area.idArea');
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}
	
	public function save($pedido){
		$this->db->set( $pedido )->insert("actapedido");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
        return NULL;
	}
	public function update($pedidoId, $data){
		$this->db->set($data)->where("idActa",$pedidoId)->update("actapedido");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($pedidoId){
		$this->db->where("idActa", $pedidoId)->delete("actapedido");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}