<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cargo_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function get($cargoId = NULL){
		if(! is_null($cargoId)){
			$query = $this->db->select("*")->from('cargo')->where("idCargo", $cargoId)->get();
			return $query->result_array();
		}else{
			$query = $this->db->select("*")->from('cargo')->get();
			if($query->num_rows()>0){
				return $query->result_array();
			}
		}
	}
	public function save($cargo){
        $query = $this->db->select("*")->from('cargo')->where("idCargo", $cargo["idCargo"])->get();
        if($query->num_rows() != 1 ){
            $this->db->set( $cargo )->insert("cargo");
            if($this->db->affected_rows() === 1 ){
                return TRUE;
            }
            return NULL;
        }
        return FALSE;
	}
	public function update($cargoId, $data){
		$this->db->set($data)->where("idCargo",$cargoId)->update("cargo");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($cargoId){
		$this->db->where("idCargo", $cargoId)->delete("cargo");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}