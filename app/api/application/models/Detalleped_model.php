<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Detalleped_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function get($actaId = NULL){
		$this->db->select('*');
		$this->db->from('detallePedido');
		if(! is_null($actaId)){
			$this->db->where('iddetallePedido',$actaId);
		}
		$this->db->join('unidad', 'detallePedido.idUnidad = unidad.idUnidad');
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}
	public function save($detalleped){
        $query = $this->db->select("*")->from('detallePedido')->where("idDetalle", $detalleped["idDetalle"])->get();
        if($query->num_rows() != 1 ){
            $this->db->set( $detalleped )->insert("detallePedido");
            if($this->db->affected_rows() === 1 ){
                return TRUE;
            }
            return NULL;
        }
        return FALSE;
	}
	public function update($detallepedId, $data){
		$this->db->set($data)->where("idDetalle",$detallepedId)->update("detallePedido");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($detallepedId){
		$this->db->where("idDetalle", $detallepedId)->delete("detallePedido");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}