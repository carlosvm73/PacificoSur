<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Incidente_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function get($actaId = NULL){
		$this->db->select('*');
		$this->db->from('incidenteTecnico');
		if(! is_null($actaId)){
			$this->db->where('idincidenteTecnico',$actaId);
		}
		$this->db->join('mantenimiento', 'incidenteTecnico.idMantenimiento = mantenimiento.idMantenimiento');
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}
	// public function get($incidenteId = NULL){
	// 	if(! is_null($incidenteId)){
	// 		$query = $this->db->select("*")->from('incidenteTecnico')->where("idIncidente", $incidenteId)->get();
	// 		return $query->result_array();
	// 	}else{
	// 		$query = $this->db->select("*")->from('incidenteTecnico')->get();
	// 		if($query->num_rows()>0){
	// 			return $query->result_array();
	// 		}
	// 	}
	// }
	public function save($incidente){
		$this->db->set( $incidente )->insert("incidenteTecnico");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;
	}
	public function update($incidenteId, $data){
		$this->db->set($data)->where("idIncidente",$incidenteId)->update("incidenteTecnico");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($incidenteId){
		$this->db->where("idIncidente", $incidenteId)->delete("incidenteTecnico");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}