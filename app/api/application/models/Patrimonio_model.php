<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Patrimonio_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function get($actaId = NULL){
		$this->db->select('*');
		$this->db->from('patrimonio');
		if(! is_null($actaId)){
			$this->db->where('idpatrimonio',$actaId);
		}
		$this->db->join('personal', 'patrimonio.dni_personal = personal.dni_personal');
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}
	public function save($patrimonio){
        // $query = $this->db->select("*")->from('patrimonio')->where("idPatrimonio", $patrimonio["idPatrimonio"])->get();
        // if($query->num_rows() != 1 ){
        // }
        // return FALSE;
		$this->db->set( $patrimonio )->insert("patrimonio");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;
	}
	public function update($patrimonioId, $data){
		$this->db->set($data)->where("idPatrimonio",$patrimonioId)->update("patrimonio");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($patrimonioId){
		$this->db->where("idPatrimonio", $patrimonioId)->delete("patrimonio");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}