<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function verify($user){
		$this->db->select('*');
		$this->db->from('usuario');
		$this->db->where('user_name',$user["user_name"]);
		$this->db->join('personal', 'usuario.dni_personal = personal.dni_personal');
		$this->db->join('cargo', 'personal.idCargo = cargo.idCargo');
		$this->db->join('area', 'personal.idArea = area.idArea');
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}
	// public function get($userId = NULL){
	// 	$this->db->select('*');
	// 	$this->db->from('personal');
	// 	if(! is_null($userId)){
	// 		$this->db->where('dni_personal',$userId);
	// 	}
	// 	$this->db->join('usuario', 'personal.dni_personal = usuario.dni_personal');
	// 	$query = $this->db->get();
	// 	if($query->num_rows()>0){
	// 		return $query->result_array();
	// 	}
	// }
	public function get($userId = NULL){
		$this->db->select('*');
		$this->db->from('usuario');
		if(! is_null($userId)){
			$this->db->where('dni_personal',$userId);
		}
		$this->db->join('personal', 'usuario.dni_personal = personal.dni_personal');
		$this->db->join('cargo', 'personal.idCargo = cargo.idCargo');
		$this->db->join('area', 'personal.idArea = area.idArea');
		$query = $this->db->get();
		return $query->result_array();
	}
	public function save($user){
        $query = $this->db->select("*")->from('usuario')->where("dni_personal", $user["dni_personal"])->get();
        if($query->num_rows() != 1 ){
            $this->db->set( $user )->insert("usuario");
            if($this->db->affected_rows() === 1 ){
                return TRUE;
            }
            return NULL;
        }
        return FALSE;
	}
	public function update($userId, $data){
		$this->db->set($data)->where("dni_personal",$userId)->update("usuario");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;
	}
	public function delete($userId){
		$this->db->where("dni_personal", $userId)->delete("usuario");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}