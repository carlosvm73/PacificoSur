<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Acta_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function get($actaId = NULL){
		$this->db->select('*');
		$this->db->from('actaconformidadservicio');
		if(! is_null($actaId)){
			$this->db->where('nro_identificacion',$actaId);
		}
		$this->db->join('personal', 'actaconformidadservicio.dni_personal = personal.dni_personal');
		$this->db->join('cliente', 'actaconformidadservicio.dni_cliente = cliente.dni_cliente');
		$this->db->join('proveedor', 'actaconformidadservicio.idProveedor = proveedor.idProveedor');
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}
	public function save($acta){
        // $query = $this->db->select("*")->from('actaconformidadservicio')->where("nro_identificacion", $acta["nro_identificacion"])->get();
        // if($query->num_rows() != 1 ){
        // }
        // return FALSE;
		$this->db->set( $acta )->insert("actaconformidadservicio");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;
	}
	public function update($actaId, $data){
		$this->db->set($data)->where("nro_identificacion",$actaId)->update("actaconformidadservicio");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($actaId){
		$this->db->where("nro_identificacion", $actaId)->delete("actaconformidadservicio");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}