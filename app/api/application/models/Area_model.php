<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Area_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function get($areaId = NULL){
		if(! is_null($areaId)){
			$query = $this->db->select("*")->from('area')->where("idArea", $areaId)->get();
			return $query->result_array();
		}else{
			$query = $this->db->select("*")->from('area')->get();
			if($query->num_rows()>0){
				return $query->result_array();
			}
		}
	}
	public function save($area){
        $query = $this->db->select("*")->from('area')->where("idArea", $area["idArea"])->get();
        if($query->num_rows() != 1 ){
            $this->db->set( $area )->insert("area");
            if($this->db->affected_rows() === 1 ){
                return TRUE;
            }
            return NULL;
        }
        return FALSE;
	}
	public function update($areaId, $data){
		$this->db->set($data)->where("idArea",$areaId)->update("area");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($areaId){
		$this->db->where("idArea", $areaId)->delete("area");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}