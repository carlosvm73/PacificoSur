<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Activo_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}	public function get($actaId = NULL){
		$this->db->select('*');
		$this->db->from('activo');
		if(! is_null($actaId)){
			$this->db->where('idactivo',$actaId);
		}
		$this->db->join('patrimonio', 'activo.idPatrimonio = patrimonio.idPatrimonio');
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}
	public function save($activo){
        // $query = $this->db->select("*")->from('activo')->where("idActivo", $activo["idActivo"])->get();
        // if($query->num_rows() != 1 ){
        // }
        // return FALSE;
            $this->db->set( $activo )->insert("activo");
            if($this->db->affected_rows() === 1 ){
                return TRUE;
            }
            return NULL;
	}
	public function update($activoId, $data){
		$this->db->set($data)->where("idActivo",$activoId)->update("activo");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($activoId){
		$this->db->where("idActivo", $activoId)->delete("activo");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}