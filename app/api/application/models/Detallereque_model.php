<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Detallereque_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function get($actaId = NULL){
		$this->db->select('*');
		$this->db->from('detalleRequerimiento');
		if(! is_null($actaId)){
			$this->db->where('iddetalleRequerimiento',$actaId);
		}
		$this->db->join('unidad', 'detalleRequerimiento.idUnidad = unidad.idUnidad');
		$this->db->join('bien', 'detalleRequerimiento.idBien = bien.idBien');
		$this->db->join('requerimientobienes', 'detalleRequerimiento.idRequerimiento = requerimientobienes.idRequerimiento');
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}

	public function save($detallereque){
        $query = $this->db->select("*")->from('detalleRequerimiento')->where("idDetalle", $detallereque["idDetalle"])->get();
        if($query->num_rows() != 1 ){
            $this->db->set( $detallereque )->insert("detalleRequerimiento");
            if($this->db->affected_rows() === 1 ){
                return TRUE;
            }
            return NULL;
        }
        return FALSE;
	}
	public function update($detallereqId, $data){
		$this->db->set($data)->where("idDetalle",$detallereqId)->update("detalleRequerimiento");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($detallereqId){
		$this->db->where("idDetalle", $detallereqId)->delete("detalleRequerimiento");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}