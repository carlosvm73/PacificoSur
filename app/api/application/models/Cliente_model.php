<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function get($clienteId = NULL){
		if(! is_null($clienteId)){
			$query = $this->db->select("*")->from('cliente')->where("idCliente", $clienteId)->get();
			return $query->result_array();
		}else{
			$query = $this->db->select("*")->from('cliente')->get();
			if($query->num_rows()>0){
				return $query->result_array();
			}
		}
	}
	public function save($cliente){
        // $query = $this->db->select("*")->from('cliente')->where("idCliente", $cliente["idCliente"])->get();
        // if($query->num_rows() != 1 ){
        // }
        // return FALSE;
            $this->db->set( $cliente )->insert("cliente");
            if($this->db->affected_rows() === 1 ){
                return TRUE;
            }
            return NULL;
	}
	public function update($clienteId, $data){
		$this->db->set($data)->where("idCliente",$clienteId)->update("cliente");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($clienteId){
		$this->db->where("idCliente", $clienteId)->delete("cliente");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}