<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Unidad_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function get($unidadId = NULL){
		if(! is_null($unidadId)){
			$query = $this->db->select("*")->from('unidad')->where("idUnidad", $unidadId)->get();
			return $query->result_array();
		}else{
			$query = $this->db->select("*")->from('unidad')->get();
			if($query->num_rows()>0){
				return $query->result_array();
			}
		}
	}
	public function save($unidad){
		$this->db->set( $unidad )->insert("unidad");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
        return NULL;
	}
	public function update($unidadId, $data){
		$this->db->set($data)->where("idUnidad",$unidadId)->update("unidad");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
		return NULL;

	}
	public function delete($unidadId){
		$this->db->where("idUnidad", $unidadId)->delete("unidad");
		if($this->db->affected_rows() === 1 ){
			return TRUE;
		}
	}

}