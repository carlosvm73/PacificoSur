'use strict';

angular.module('clientApp')
  .constant('SideNavItems', [
    // { title: 'Dashboard', icon: 'event_seat' },
    { title: 'Patrimonio', icon: 'account_balance' },
    // { title: 'Personal', icon: 'group' },
    { title: 'Requerimientos', icon: 'business' },
    { title: 'Mantenimientos', icon: 'attach_file' },
    { title: 'Conformidad', icon: 'network_check' },
    { title: 'Pedidos', icon: 'content_paste' },
    { title: 'Proveedores', icon: 'transfer_within_a_station' },
    { title: 'Clientes', icon: 'contacts' },
    { title: 'Configuración', icon: 'tune' }, // unidad de medida, areas, cargos, bienes, tipo activo
  ])
  .config(function ($mdThemingProvider) {
    $mdThemingProvider.theme('indigo')
      .primaryPalette('indigo')
      .accentPalette('pink');

    $mdThemingProvider.theme('lime')
      .primaryPalette('lime')
      .accentPalette('orange')
      .warnPalette('blue');
    $mdThemingProvider.alwaysWatchTheme(true);
  })
  .controller('HomeCtrl', function ($scope, ContentData, resource, $http, $cookieStore, $routeParams, $location, $mdDialog, SideNavItems, $mdSidenav, $mdToast, $mdBottomSheet, $mdMenu, $timeout, $log) {

    $scope.items = SideNavItems;
    $scope.openMenu = function ($mdMenu, ev) {
      $mdMenu.open(ev);
    };

    $scope.logout = function () {
      $cookieStore.remove('token');
      $cookieStore.remove('usuario');
      $cookieStore.remove('grupo');
      $cookieStore.remove('nombres');
      $cookieStore.remove('paterno');
      $cookieStore.remove('materno');
      $cookieStore.remove('dni');
      $location.path('/');
    }

    //cargando usuarios
    $scope.currentUser = {
      uid: $cookieStore.get('token'),
      email: $cookieStore.get('usuario'),
      // email: '$cookieStore.get',
      grupo: $cookieStore.get('grupo'),
      photo: 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?f=y',
      name: $cookieStore.get('nombres'),
      id: $cookieStore.get('dni'),
    };

    //toogle Navbar
    $scope.MenuBar = buildToggler('left');
    function buildToggler(componentId) {
      return  ()=>$mdSidenav(componentId).toggle();
    };

    //redirect to template original
    $scope.isActive = destination => destination === $scope.items[0].title;
    $scope.link = function (index, title) {
      // if(index === 6 && $scope.currentUser.grupo !== 'Jefe') return;
      $scope.viewTab = { key: index, value: title };
      $scope.isActive = destination => destination === title;
    };

    //CUENTAS
    $http.get('/api/index.php/user').then(response => {
      // console.log(response.data);
      ContentData.setDataAccount(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //ACTA 
    $http.get('/api/index.php/acta').then(response => {
      // console.log(response.data);
      ContentData.setDataActa(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //ACTIVO
    $http.get('/api/index.php/activo').then(response => {
      // console.log(response.data);
      ContentData.setDataActivo(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //AREA 
    $http.get('/api/index.php/area').then(response => {
      // console.log(response.data);
      ContentData.setDataArea(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //BIEN 
    $http.get('/api/index.php/bien').then(response => {
      // console.log(response.data);
      ContentData.setDataBien(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //CARGO 
    $http.get('/api/index.php/cargo').then(response => {
      // console.log(response.data);
      ContentData.setDataCargo(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //CLIENTE 
    $http.get('/api/index.php/cliente').then(response => {
      // console.log(response.data);
      ContentData.setDataCliente(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //DETALLEPED 
    $http.get('/api/index.php/detalleped').then(response => {
      // console.log(response.data);
      ContentData.setDataDetalleped(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //DETALLEREQUE
    $http.get('/api/index.php/detallereque').then(response => {
      // console.log(response.data);
      ContentData.setDataDetallereque(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //INCIDENTE 
    $http.get('/api/index.php/incidente').then(response => {
      // console.log(response.data);
      ContentData.setDataIncidente(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //MANTENIMIENTO 
    $http.get('/api/index.php/mantenimiento').then(response => {
      // console.log(response.data);
      ContentData.setDataMantenimiento(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //PEDIDO 
    $http.get('/api/index.php/pedido').then(response => {
      // console.log(response.data);
      ContentData.setDataPedido(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //PROVEEDOR 
    $http.get('/api/index.php/proveedor').then(response => {
      // console.log(response.data);
      ContentData.setDataProveedor(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //REQUERIMIENTO 
    $http.get('/api/index.php/requerimiento').then(response => {
      // console.log(response.data);
      ContentData.setDataRequerimiento(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //TAREA 
    $http.get('/api/index.php/tarea').then(response => {
      // console.log(response.data);
      ContentData.setDataTarea(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //UNIDAD 
    $http.get('/api/index.php/unidad').then(response => {
      // console.log(response.data);
      ContentData.setDataUnidad(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //PATRIMONIO
    $http.get('/api/index.php/patri').then(response => {
      // console.log(response.data);
      ContentData.setDataPatrimonio(response.data);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });
    //PERSONAL 
    $http.get('/api/index.php/personal').then(response => {
      // console.log(response.data);
      let ff=response.data.map(value=>{
        value.account = ContentData.getDataAccount().filter(val=>val.dni_personal===value.dni_personal).length > 0 ? true:false;
        return value;
      });
      ContentData.setDataUsuarios(ff);
    }).catch(function (response) { $log.error(response.data.message); message($mdToast, response.data.message); });


  }) // END - HomeCTRL

  .factory('ContentData', function () {
    let config = {
      contentActa: undefined,
      setDataActa: x => { config.contentActa = x; },
      getDataActa: () => config.contentActa,
      contentActivo: undefined,
      setDataActivo: x => { config.contentActivo = x; },
      getDataActivo: () => config.contentActivo,
      contentArea: undefined,
      setDataArea: x => { config.contentArea = x; },
      getDataArea: () => config.contentArea,
      contentBien: undefined,
      setDataBien: x => { config.contentBien = x; },
      getDataBien: () => config.contentBien,
      contentCargo: undefined,
      setDataCargo: x => { config.contentCargo = x; },
      getDataCargo: () => config.contentCargo,
      contentCliente: undefined,
      setDataCliente: x => { config.contentCliente = x; },
      getDataCliente: () => config.contentCliente,
      contentDetalleped: undefined,
      setDataDetalleped: x => { config.contentDetalleped = x; },
      getDataDetalleped: () => config.contentDetalleped,
      contentDetallereque: undefined,
      setDataDetallereque: x => { config.contentDetallereque = x; },
      getDataDetallereque: () => config.contentDetallereque,
      contentIncidente: undefined,
      setDataIncidente: x => { config.contentIncidente = x; },
      getDataIncidente: () => config.contentIncidente,
      contentMantenimiento: undefined,
      setDataMantenimiento: x => { config.contentMantenimiento = x; },
      getDataMantenimiento: () => config.contentMantenimiento,
      contentPatrimonio: undefined,
      setDataPatrimonio: x => { config.contentPatrimonio = x; },
      getDataPatrimonio: () => config.contentPatrimonio,
      contentPedido: undefined,
      setDataPedido: x => { config.contentPedido = x; },
      getDataPedido: () => config.contentPedido,
      contentUsuarios: undefined,
      setDataUsuarios: x => { config.contentUsuarios = x; },
      getDataUsuarios: () => config.contentUsuarios,
      contentProveedor: undefined,
      setDataProveedor: x => { config.contentProveedor = x; },
      getDataProveedor: () => config.contentProveedor,
      contentRequerimiento: undefined,
      setDataRequerimiento: x => { config.contentRequerimiento = x; },
      getDataRequerimiento: () => config.contentRequerimiento,
      contentTarea: undefined,
      setDataTarea: x => { config.contentTarea = x; },
      getDataTarea: () => config.contentTarea,
      contentUnidad: undefined,
      setDataUnidad: x => { config.contentUnidad = x; },
      getDataUnidad: () => config.contentUnidad,
      contentAccount: undefined,
      setDataAccount: x => { config.contentAccount = x; },
      getDataAccount: () => config.contentAccount
    }
    return config;
  });
function message($mdToast, msj) {
  $mdToast.show(
    $mdToast.simple()
      .textContent(msj)
      .position('bottom right')
      .action('ok')
      .hideDelay(5000));
}
