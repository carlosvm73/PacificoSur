'use strict';

/**
 * @ngdoc function|
 * @name clientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('LoginCtrl', function ($scope, $cookieStore, $location, $log,  $mdToast, $timeout, $resource, $http) {
    var FormActive={
        active:{'display':'block'},progress:{'display':'none'},
        button:false,title:'Login'};
    $scope.SignIn=function(data){
      $scope.form={
        active:{'display':'none'},progress:{'display':'block'},
        button: true,title: 'Validando ...'};
        $http.post('/api/index.php/auth/signin', {
          "user_name":data.user_name,
          "user_password":data.user_password
        })
        .then(function(response){
          if(response.data.access){
            // console.log(response);
            $cookieStore.put('token',response.data.token);
            $cookieStore.put('usuario',response.data.data.user_name);
            $cookieStore.put('grupo',response.data.data.user_type);
            $cookieStore.put('nombres',response.data.data.nombres_personal);
            $cookieStore.put('paterno',response.data.data.primer_apellido_personal);
            $cookieStore.put('materno',response.data.data.segundo_apellido_personal);
            $cookieStore.put('dni',response.data.data.dni_personal);
            $location.path('/home');
            $log.info(response.data.message);
          }else{
          $log.warn(response.data.message);
          $mdToast.show(
            $mdToast.simple()
            .textContent(response.data.message)
            .position('bottom right')
            .action('ok')
            .hideDelay(5000));
          }
        }).catch(function(response){
          $log.error(response.data.message);
          $mdToast.show(
            $mdToast.simple()
            .textContent(response.data.message)
            .position('bottom right')
            .action('ok')
            .hideDelay(5000));
        });
        $scope.form=FormActive;

    }
  });
