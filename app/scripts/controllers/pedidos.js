'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:PedidosCtrl
 * @description
 * # PedidosCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('PedidosCtrl', function ($scope, ContentData, $http, $timeout, $mdToast, $mdDialog, $log) {

    $scope.$watch(function () {
      return ContentData.getDataPedido();
    }, function (value) {
      if (value !== undefined) {
        $scope.Collection_pedido = value;
      }
    });

    $scope.$watch(function () {
      return ContentData.getDataArea();
    }, function (value) {
      if (value !== undefined) {
        $scope.AreasList = value;
      }
    });

    $scope.cancerToEditPedido = function () {
      $scope.editPedido = 'Nuevo';
      $scope.pedidoItem.selectedIndex = 0;
      $scope.pedido = {};
    }

    $scope.Pass_Pedido = function (op, data, ev) {
      if (op === 'modify') {
        $scope.editPedido = 'Editar';
        $scope.pedidoItem.selectedIndex = 1;
        $scope.pedido = data;
      } else if (op === 'delete') {
        var confirm = $mdDialog.confirm()
          .title(`Desea Eliminar el Pedido ${data.idActa}?`)
          .ariaLabel('pedido')
          .targetEvent(ev)
          .ok('Eliminar')
          .cancel('Cancelar');

        $mdDialog.show(confirm).then(function () {

          $scope.loadingPedido = true;
          $http.delete('/api/index.php/pedido/' + data.idActa).then(function (response) {
            ContentData.setDataPedido(response.data);
            $timeout(function () {
              $scope.loadingPedido = false;
              message($mdToast, 'Pedido eliminado satisfactoriamente');
            }, 1500);
          }).catch(function (response) {
            message($mdToast, 'Ha ocurrido un Error');
            $scope.loadingPedido = false;
          });

        }, function () {
          $log.info('Operacion cancelada');
        });

      } else {

      }
    }

    $scope.Save_Pedido = function (data) {
      $scope.loadingPedido = true;
      $http.post('/api/index.php/pedido', {
        fecha: data.fecha,
        idArea: data.idArea,
        motivo: data.motivo,
        tipo_uso: data.tipo_uso
      }).then(function (response) {
        ContentData.setDataPedido(response.data);
        $timeout(function () {
          $scope.pedido = {};
          message($mdToast, 'Pedido registrado satisfactoriamente');
          $scope.pedidoItem.selectedIndex = 0;
          $scope.loadingPedido = false;
        }, 1500);
      }).catch(function (response) {
        message($mdToast, response);
        $scope.loadingPedido = false;
      });
      $scope.pedidoItem.selectedIndex = 0;
    }

    $scope.Update_Pedido = function (data) {
      $scope.loadingPedido = true;
      $http.put('/api/index.php/pedido/' + data.idActa, {
        fecha: data.fecha,
        idActa: data.idActa,
        idArea: data.idArea,
        motivo: data.motivo,
        tipo_uso: data.tipo_uso
      }).then(function (response) {
        ContentData.setDataPedido(response.data);
        $timeout(function () {
          $scope.pedido = {};
          message($mdToast, 'Pedido actualizado satisfactoriamente');
          $scope.pedidoItem.selectedIndex = 0;
          $scope.loadingPedido = false;
        }, 1500);
      }).catch(function (response) {
        message($mdToast, response.data.message);
        $scope.loadingPedido = false;
      });
      $scope.editPedido = 'Nuevo';

    }

  });