'use strict';

angular.module('clientApp')
  .controller('RequerimientoCtrl', function ($scope, ContentData, $timeout, $http, $mdToast, $mdDialog, $log) {


    $scope.$watch(function () {
      return ContentData.getDataRequerimiento();
    }, function (value) {
      if (value !== undefined) {
        $scope.Collection_requerimiento = value.map(value => {
          value.Area = ContentData.getDataArea().filter(val => val.idArea === value.idArea)[0].descripcion_area;
          value.Cargo = ContentData.getDataCargo().filter(val => val.idCargo === value.idCargo)[0].descripcion_cargo;
          return value;
        })
      }
    });


    $scope.$watch(function () {
      return ContentData.getDataUnidad();
    }, function (value) {
      if (value !== undefined) {
        $scope.listUnidad = value;
      }
    });


    $scope.$watch(function () {
      return ContentData.getDataBien();
    }, function (value) {
      if (value !== undefined) {
        $scope.listBien = value;
      }
    });

    $scope.cancerToEditRequerimiento = function () {
      $scope.editDetalleReque = true;
      $scope.requerimiento = {};
      $scope.requerimientoItem.selectedIndex = 0;
      $scope.editRequerimiento = 'Nuevo';
    }
    $scope.cancerToEditDetalleReque = function () {
      $scope.requerimientoItem.selectedIndex = 0;
      $scope.seeDetalleReque = false;
      $scope.editDetalleReque = 'Nuevo';
    }

    $scope.Pass_Requerimiento = function (op, item, ev) {
      if (op === 'modify') {
        $scope.editDetalleReque = true;
        $scope.requerimiento = item;
        $scope.requerimientoItem.selectedIndex = 1;
        $scope.editRequerimiento = 'Editar';
      } else if (op === 'bienes') {
        // console.log(item);
        $scope.detalleReque = {
          idRequerimiento: item.idRequerimiento
        };
        $scope.requerimientoItem.selectedIndex = 1;
        $scope.seeDetalleReque = true;
        $scope.editDetalleReque = 'Editar';

        // $scope.seeBien=true;
        // $scope.editRequerimiento='Nuevo';
        // $scope.editBien=true;
      } else if (op === 'historial') {
        $scope.viewDetalleReque = true;
        item.historial = ContentData.getDataDetallereque().filter(value => value.idRequerimiento === item.idRequerimiento);
        $scope.detailsRequerimiento = item;
        $scope.requerimientoItem.selectedIndex = 2;
        // console.log("historial", item);
      } else if (op === 'delete') {
        var confirm = $mdDialog.confirm()
          .title(`Desea Eliminar el Requerimiento ${item.idRequerimiento}?`)
          .ariaLabel('Requerimiento')
          .targetEvent(ev)
          .ok('Eliminar')
          .cancel('Cancelar');

        $mdDialog.show(confirm).then(function () {

          $scope.loadingRequerimiento = true;
          $http.delete('/api/index.php/requerimiento/' + item.idRequerimiento).then(function (response) {
            ContentData.setDataRequerimiento(response.data);
            $timeout(function () {
              $scope.loadingRequerimiento = false;
              message($mdToast, 'Requerimiento eliminado satisfactoriamente');
            }, 1500);
          }).catch(function (response) {
            message($mdToast, 'Ha ocurrido un Error');
            $scope.loadingRequerimiento = false;
          });

        }, function () {
          $log.info('Operacion cancelada');
        });

      }

    }

    $scope.Save_Requerimiento = function (data) {
      $scope.editDetalleReque = false;
      $scope.loadingRequerimiento = true;
      // console.log(data);
      $http.post('/api/index.php/requerimiento', data).then(function (response) {
        ContentData.setDataRequerimiento(response.data);
        $timeout(function () {
          $scope.loadingRequerimiento = false;
          message($mdToast, 'Requerimiento registrado satisfactoriamente');
        }, 1500);
      }).catch(function (response) {
        message($mdToast, 'Ha ocurrido un Error');
        $scope.loadingRequerimiento = false;
      });
    }

    $scope.Update_Requerimiento = function (data) {
      $scope.editDetalleReque = true;
      $scope.loadingRequerimiento = true;
      // console.log(data);
      $http.put('/api/index.php/requerimiento/' + data.idRequerimiento, data).then(function (response) {
        ContentData.setDataRequerimiento(response.data);
        $timeout(function () {
          $scope.loadingRequerimiento = false;
          message($mdToast, 'Reuquerimiento actualizado satisfactoriamente');
        }, 1500);
      }).catch(function (response) {
        message($mdToast, 'Ha ocurrido un Error');
        $scope.loadingRequerimiento = false;
      });
    }

    $scope.Save_DetalleReque = function (data) {
      // console.log(data);
      $scope.loadingRequerimiento = true;
      $http.post('/api/index.php/detallereque', data).then(function (response) {
        ContentData.setDataDetallereque(response.data);
        $timeout(function () {
          $scope.loadingRequerimiento = false;
          message($mdToast, 'Detalle de Requerimiento registrado satisfactoriamente');
        }, 1500);
      }).catch(function (response) {
        message($mdToast, 'Ha ocurrido un Error');
        $scope.loadingRequerimiento = false;
      });
    }



  })