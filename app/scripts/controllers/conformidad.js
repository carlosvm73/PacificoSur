'use strict';

angular.module('clientApp')
  .controller('ConformidadCtrl', function ($scope, ContentData, $http, $mdToast, $timeout, $mdDialog, $log, $filter) {

    $scope.$watch(function () {
      return ContentData.getDataActa();
    }, function (value) {
      if (value !== undefined) {
        $scope.Collection_conformidad = value;
        // console.log('ActasDeConformidad', value);
      }
    });

    $scope.$watch(function () {
      return ContentData.getDataProveedor();
    }, function (value) {
      if (value !== undefined) {
        $scope.proveedores = value;
        // console.log('ActasDeConformidad', value);
      }
    });

    $scope.filterToClient = function (search) {
      let result = ContentData.getDataCliente().filter(value => value.dni_cliente === search);
      if (result.length > 0) {
        return result[0];
      }
    }

    $scope.cancerToEditConformidad=function(){
        $scope.editConformidad='Nuevo';
        $scope.acta={};
        $scope.conformidadItem.selectedIndex=0;
    }

    $scope.Pass_Conformidad = function (op, item, ev) {
      if (op === 'modify') {
        $scope.acta=item;
        $scope.editConformidad='Editar';
      } else if (op === 'delete') {
        var confirm = $mdDialog.confirm()
          .title(`Desea Eliminar esta Acta de Conformidad ?`)
          .textContent(`Acta  ${item.concepto} N° ${item.nro_identificacion}`)
          .ariaLabel('Acta de Conformidad')
          .targetEvent(ev)
          .ok('Eliminar')
          .cancel('Cancelar');

        $mdDialog.show(confirm).then(function () {

          $scope.loadingConformidad = true;
          $http.delete('/api/index.php/acta/' + item.nro_identificacion).then(function (response) {
            ContentData.setDataActa(response.data);
            $timeout(function () {
              $scope.loadingConformidad = false;
              message($mdToast, 'Acta de Conformidad eliminado satisfactoriamente');
            }, 1500);
          }).catch(function (response) {
            message($mdToast, 'Ha ocurrido un Error');
            $scope.loadingConformidad = false;
          });

        }, function () {
          $log.info('Operacion cancelada');
        });

      }
    }

    $scope.Save_Acta = function (data) {
      data.dni_personal = $scope.currentUser.id;
      let identifyClient = ContentData.getDataCliente().filter(value => value.dni_cliente === data.dni_cliente).length > 0 ? true : false;
      console.log(data);
      console.log(identifyClient);
      $scope.loadingConformidad = true;
      if (!identifyClient) {
        $http.post('/api/index.php/cliente', {
          nombres_cliente: data.nombres_cliente,
          primer_apellido_cliente: data.primer_apellido_cliente,
          segundo_apellido_cliente: data.segundo_apellido_cliente,
          dni_cliente: data.dni_cliente
        }).then(function (response) {
          ContentData.setDataCliente(response.data);
          $timeout(function () {
            message($mdToast, 'Cliente registrado satisfactoriamente');
          }, 1500);
        }).catch(function (response) {
          message($mdToast, 'Ha ocurrido un Error');
          $scope.loadingConformidad = false;
        });

      }
      $http.post('/api/index.php/acta', {
        concepto: data.concepto,
        nro_contrato: data.nro_contrato,
        nro_os: data.nro_os,
        nro_doc_ref: data.nro_doc_ref,
        descripcion_acta: data.descripcion_acta,
        descripcion_servicio: data.descripcion_servicio,
        dni_cliente: data.dni_cliente,
        dni_personal: data.dni_personal,
        fecha_conformidad: data.fecha_conformidad,
        idProveedor: data.idProveedor,
        monto_con_conformidad: data.monto_con_conformidad,
        monto_registro_conformidad: data.monto_registro_conformidad,
        monto_saldo: data.monto_saldo,
        estado: data.estado,

      }).then(function (response) {
        ContentData.setDataActa(response.data);
        $timeout(function () {
          $scope.loadingConformidad = false;
          message($mdToast, 'Acta de Conformidad registrada satisfactoriamente');
          $scope.conformidadItem.selectedIndex = 0;
        }, 1500);
      }).catch(function (response) {
        message($mdToast, 'Ha ocurrido un Error');
        $scope.loadingConformidad = false;
      });

    }

    $scope.Update_Acta = function (data) {
      $scope.loadingConformidad = true;
      // $scope.editCuenta = true;
      $http.put('/api/index.php/acta/' + data.nro_identificacion, {
        nro_identificacion:data.nro_identificacion,
        concepto: data.concepto,
        nro_contrato: data.nro_contrato,
        nro_os: data.nro_os,
        nro_doc_ref: data.nro_doc_ref,
        descripcion_acta: data.descripcion_acta,
        descripcion_servicio: data.descripcion_servicio,
        dni_cliente: data.dni_cliente,
        dni_personal: data.dni_personal,
        fecha_conformidad: data.fecha_conformidad,
        idProveedor: data.idProveedor,
        monto_con_conformidad: data.monto_con_conformidad,
        monto_registro_conformidad: data.monto_registro_conformidad,
        monto_saldo: data.monto_saldo,
        estado: data.estado,
      }).then(function (response) {
        ContentData.setDataActa(response.data);
        $timeout(function () {
          $scope.loadingConformidad = false;
          $scope.conformidadItem.selectedIndex = 0;
          message($mdToast, 'Acta de Conformidad actualizada satisfactoriamente');
        }, 1500);
      }).catch(function (response) {
        message($mdToast, 'Ha ocurrido un Error');
        $scope.loadingConformidad = false;
      });
    }

  });