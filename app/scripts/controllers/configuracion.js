'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:ConfiguracionCtrl
 * @description
 * # ConfiguracionCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('ConfiguracionCtrl', function ($scope, ContentData, $http, $mdToast, $mdDialog, $log) {

    $scope.$watch(function () {
      return ContentData.getDataArea();
    }, function (value) {
      if (value !== undefined) {
        $scope.Collection_Area = value;
      }
    });
    $scope.$watch(function () {
      return ContentData.getDataBien();
    }, function (value) {
      if (value !== undefined) {
        $scope.Collection_Bien = value;
      }
    });
    $scope.$watch(function () {
      return ContentData.getDataUnidad();
    }, function (value) {
      if (value !== undefined) {
        $scope.Collection_Unidad = value;
      }
    });
    $scope.$watch(function () {
      return ContentData.getDataCargo();
    }, function (value) {
      if (value !== undefined) {
        $scope.Collection_Cargo = value;
      }
    });

    $scope.cancelArea=function(){
      $scope.editArea= false;
      $scope.area={};
    }
    $scope.passArea = function (op, item) {
      $scope.editArea= true;
      $scope.area=item;
    }
    $scope.cancelUnidad=function(){
      $scope.editUnidad= false;
      $scope.unidad={};
    }

    $scope.passUnidad = function (op, item) {
      $scope.editUnidad= true;
      $scope.unidad=item;
    }
    $scope.cancelCargo=function(){
      $scope.editCargo= false;
      $scope.cargo={};
    }
    $scope.passCargo = function (op, item) {
      $scope.editCargo= true;
      $scope.cargo=item;
    }
    $scope.cancelBien=function(){
      $scope.editBien= false;
      $scope.bien={};
    }
    $scope.passBien = function (op, item) {
      $scope.editBien= true;
      $scope.bien=item;
    }
    $scope.edit = function (op, data) {
      switch (op) {
        case 'Bien':
          $http.put('/api/index.php/bien/' + data.idBien, {
            descripcion_bien:data.descripcion_bien,
            tipoTangintang:data.tipoTangintang
          }).then(response => {
            $http.get('/api/index.php/bien').then(response => {
              ContentData.setDataBien(response.data);
            }).catch(function (response) {
              $log.error(response.data.message);
              message($mdToast, response.data.message);
            });
          }).catch(function (response) {
            $log.error(response.data.message);
            message($mdToast, response.data.message);
          });
          $scope.editBien= false;
          $scope.bien={};
          break;
        case 'Cargo':
          $http.put('/api/index.php/cargo/' + data.idCargo, {
            descripcion_cargo:data.descripcion_cargo,
            idCargo:data.idCargo
          }).then(response => {
            $http.get('/api/index.php/cargo').then(response => {
              ContentData.setDataCargo(response.data);
            }).catch(function (response) {
              $log.error(response.data.message);
              message($mdToast, response.data.message);
            });
          }).catch(function (response) {
            $log.error(response.data.message);
            message($mdToast, response.data.message);
          });
          $scope.editCargo= false;
          $scope.cargo={};
          break;
        case 'Unidad':
          $http.put('/api/index.php/unidad/' + data.idUnidad, {
            descripcion_unidad:data.descripcion_unidad
          }).then(response => {
            $http.get('/api/index.php/unidad').then(response => {
              ContentData.setDataUnidad(response.data);
            }).catch(function (response) {
              $log.error(response.data.message);
              message($mdToast, response.data.message);
            });
          }).catch(function (response) {
            $log.error(response.data.message);
            message($mdToast, response.data.message);
          });
          $scope.editUnidad= false;
          $scope.unidad={};
          break;
        case 'Area':
          $http.put('/api/index.php/area/' + data.idArea, {
            descripcion_area:data.descripcion_area,
            idArea:data.idArea
          }).then(response => {
            $http.get('/api/index.php/area').then(response => {
              ContentData.setDataArea(response.data);
            }).catch(function (response) {
              $log.error(response.data.message);
              message($mdToast, response.data.message);
            });
          }).catch(function (response) {
            $log.error(response.data.message);
            message($mdToast, response.data.message);
          });
          $scope.editArea= false;
          $scope.area={};
          break;
      }
    }

    $scope.new = function (op, data) {
      switch (op) {
        case 'Bien':
          $http.post('/api/index.php/bien', data).then(response => {
            $http.get('/api/index.php/bien').then(response => {
              ContentData.setDataBien(response.data);
            }).catch(function (response) {
              $log.error(response.data.message);
              message($mdToast, response.message);
            });
          }).catch(function (response) {
            $log.error(response.message);
            message($mdToast, response.message);
          });
              $scope.bien={};
          break;
        case 'Cargo':
          $http.post('/api/index.php/cargo', data).then(response => {
            $http.get('/api/index.php/cargo').then(response => {
              ContentData.setDataCargo(response.data);
            }).catch(function (response) {
              $log.error(response.data.message);
              message($mdToast, response.message);
            });
          }).catch(function (response) {
            $log.error(response.message);
            message($mdToast, response.message);
          });
              $scope.cargo={};
          break;
        case 'Unidad':
          $http.post('/api/index.php/unidad', {
            descripcion_unidad:data.descripcion_unidad
          }).then(response => {
            $http.get('/api/index.php/unidad').then(response => {
              ContentData.setDataUnidad(response.data);
            }).catch(function (response) {
              $log.error(response.data.message);
              message($mdToast, response.message);
            });
          }).catch(function (response) {
            $log.error(response.message);
            message($mdToast, response.message);
          });
              $scope.unidad={};
          break;
        case 'Area':
          $http.post('/api/index.php/area', data).then(response => {
            $http.get('/api/index.php/area').then(response => {
              ContentData.setDataArea(response.data);
            }).catch(function (response) {
              $log.error(response.data.message);
              message($mdToast, response.message);
            });
          }).catch(function (response) {
            $log.error(response.message);
            message($mdToast, response.message);
          });
              $scope.area={};
          break;
      }
    }


  })