'use strict';
angular.module('clientApp')
  .controller('PatrimonioCtrl', function ($scope, ContentData, $timeout, $http, $mdToast, $mdDialog, $log) {

    $scope.$watch(function () {
      return ContentData.getDataPatrimonio();
    }, function (value) {
      if (value !== undefined) {
        $scope.Collection_patrimonio = value.map(value => {
          value.Area = ContentData.getDataArea().filter(val => val.idArea === value.idArea)[0].descripcion_area;
          value.Cargo = ContentData.getDataCargo().filter(val => val.idCargo === value.idCargo)[0].descripcion_cargo;
          return value;
        });
      }
    });

    $scope.$watch(function () {
      return ContentData.getDataActa();
    }, function (value) {
      if (value !== undefined) {
        $scope.Collection_conformidad = value;
        // console.log('ActasDeConformidad', value);
      }
    });

    $scope.$watch(function () {
      return ContentData.getDataArea();
    }, function (value) {
      if (value !== undefined) {
        $scope.listArea = value;
      }
    });

    // $scope.$watch(function () {
    //   return ContentData.getDataUsuarios();
    // }, function (value) {
    //   if (value !== undefined) {
    //     $scope.listPersonal = value;
    //   }
    // });

    $scope.cancerToEditPatrimonio = function () {
      $scope.editActivo = true;
      $scope.patrimonio = {};
      $scope.patrimonioItem.selectedIndex = 0;
      $scope.editPatrimonio = 'Nuevo';
    }
    $scope.cancerToEditActivo = function () {
      $scope.patrimonioItem.selectedIndex = 0;
      $scope.seeActivo = false;
      $scope.editActivo = 'Nuevo';
    }

    $scope.Pass_Patrimonio = function (op, item, ev) {
      if (op === 'modify') {
        $scope.editActivo = true;
        $scope.patrimonio = item;
        $scope.patrimonioItem.selectedIndex = 1;
        $scope.editPatrimonio = 'Editar';
      } else if (op === 'activos') {

        $scope.patrimonioItem.selectedIndex = 1;
        $scope.seeActivo = true;
        $scope.editActivo = 'Editar';
        $scope.activo={idPatrimonio:item.idPatrimonio};
      } else if (op === 'historial') {
        $scope.viewIncedencias = true;
        item.historial = ContentData.getDataActivo().filter(value => value.idPatrimonio === item.idPatrimonio);
        $scope.detailsPatrimonio = item;
        $scope.patrimonioItem.selectedIndex = 2;
      } else if (op === 'delete') {

        var confirm = $mdDialog.confirm()
          .title(`Desea Eliminar el Patrimonio ${item.idPatrimonio}?`)
          .ariaLabel('Patrimonio')
          .targetEvent(ev)
          .ok('Eliminar')
          .cancel('Cancelar');

        $mdDialog.show(confirm).then(function () {

          $scope.loadingPatrimonio = true;
          $http.delete('/api/index.php/patri/' + item.idPatrimonio).then(function (response) {
            ContentData.setDataPatrimonio(response.data);
            $timeout(function () {
              $scope.loadingPatrimonio = false;
              message($mdToast, 'Patrimonio eliminado satisfactoriamente');
            }, 1500);
          }).catch(function (response) {
            message($mdToast, 'Ha ocurrido un Error');
            $scope.loadingPatrimonio = false;
          });

        }, function () {
          $log.info('Operacion cancelada');
        });
      }

    }

    $scope.Save_Patrimonio = function (data) {
      data.dni_personal = $scope.currentUser.id;
      $scope.loadingPatrimonio = true;
      $http.post('/api/index.php/patri', data).then(function (response) {
        ContentData.setDataPatrimonio(response.data);
        $timeout(function () {
          $scope.loadingPatrimonio = false;
          message($mdToast, 'Patrimonio registrado satisfactoriamente');
        }, 1500);
      }).catch(function (response) {
        message($mdToast, 'Ha ocurrido un Error');
        $scope.loadingPatrimonio = false;
      });
    }

    $scope.Update_Patrimonio = function (data) {
      $scope.loadingPatrimonio = true;
      $scope.editActivo = true;
      $http.put('/api/index.php/patri/' + data.idPatrimonio, data).then(function (response) {
        ContentData.setDataPatrimonio(response.data);
        $timeout(function () {
          $scope.loadingPatrimonio = false;
          message($mdToast, 'Patrimonio actualizado satisfactoriamente');
        }, 1500);
      }).catch(function (response) {
        message($mdToast, 'Ha ocurrido un Error');
        $scope.loadingPatrimonio = false;
      });
    }

    $scope.Save_Activo = function (data) {
      $scope.loadingPatrimonio = true;
      $http.post('/api/index.php/activo', data).then(function (response) {
        ContentData.setDataActivo(response.data);
        $timeout(function () {
          $scope.loadingPatrimonio = false;
          message($mdToast, 'Activo registrado satisfactoriamente');
        }, 1500);
      }).catch(function (response) {
        message($mdToast, 'Ha ocurrido un Error');
        $scope.loadingPatrimonio = false;
      });
    }

  })