'use strict';

angular.module('clientApp')
    .controller('PersonalCtrl', function ($scope, ContentData, resource, $http, $cookieStore, $routeParams, $location, $mdDialog, SideNavItems, $mdSidenav, $mdToast, $mdBottomSheet, $mdMenu, $timeout, $log) {

        //toogle Navbar
        $scope.MenuBar = buildToggler('left');

        function buildToggler(componentId) {
            return function () {
                $mdSidenav(componentId).toggle();
            };
        };

        // Ctrl Usuarios
        $scope.Collection_usuarios = [];
        $scope.$watch(function () {
            return ContentData.getDataUsuarios();
        }, function (value) {
            if (value !== undefined) {
                $scope.Collection_personal = value;
                // $scope.usuariosItem.selectedIndex=0;
            }
        });
        $scope.$watch(function () {
            return ContentData.getDataArea();
        }, function (value) {
            if (value !== undefined) {
                $scope.listArea = value;
            }
        });

        $scope.$watch(function () {
            return ContentData.getDataCargo();
        }, function (value) {
            if (value !== undefined) {
                $scope.listCargo = value;
            }
        });

        $scope.myAccounts = function () {
            $scope.AllAccounts = false;
            $scope.detailsPersonal = ContentData.getDataAccount();
        }

        $scope.cancerToEditPersonal = function () {
            $scope.editCuenta = true;
            $scope.personal = {};
            $scope.personalItem.selectedIndex = 0;
            $scope.editPersonal = 'Nuevo';
        }
        $scope.cancerToEditCuenta = function () {
            $scope.personalItem.selectedIndex = 0;
            $scope.seeCuenta = false;
            $scope.editCuenta = 'Nuevo';
        }

        $scope.Pass_Personal = function (op, item, ev) {
            if (op === 'modify') {
                $scope.editCuenta = true;
                $scope.personal = item;
                $scope.personalItem.selectedIndex = 1;
                $scope.editPersonal = 'Editar';
            } else if (op === 'cuenta') {

                $scope.personalItem.selectedIndex = 1;
                $scope.seeCuenta = true;
                $scope.editCuenta = 'Editar';
                $scope.cuenta = {
                    dni_personal: item.dni_personal
                };
            } else if (op === 'historial') {
                $scope.AllAccounts = true;
                $scope.viewCuenta = true;
                item.account = ContentData.getDataAccount().filter(value => value.dni_personal === item.dni_personal);
                $scope.detailsPersonal = item;
                $scope.personalItem.selectedIndex = 2;
            } else if (op === 'delete') {
                var confirm = $mdDialog.confirm()
                    .title(`Desea Eliminar la cuenta de ${item.nombres_personal}?`)
                    .textContent('Eliminar la cuenta no elimina al usuario')
                    .ariaLabel('personal')
                    .targetEvent(ev)
                    .ok('Eliminar')
                    .cancel('Cancelar');

                $mdDialog.show(confirm).then(function () {

                    $scope.loadingPersonal = true;
                    $http.delete('/api/index.php/user/' + item.dni_personal, item).then(function (response) {
                        ContentData.setDataAccount(response.data);
                        $http.get('/api/index.php/personal').then(response => {
                            ContentData.setDataUsuarios(newReadPersonal(response.data));
                        }).catch(function (response) {
                            message($mdToast, 'Ha ocurrido un Error');
                            $scope.loadingPersonal = false;
                        });
                        $timeout(function () {
                            $scope.loadingPersonal = false;
                            message($mdToast, 'Cuenta eliminada satisfactoriamente');
                        }, 1500);
                    }).catch(function (response) {
                        message($mdToast, 'Ha ocurrido un Error');
                        $scope.loadingPersonal = false;
                    });

                }, function () {
                    $log.info('Operacion cancelada');
                });
            }

        }

        $scope.Save_Personal = function (data) {
            if (data.dni_personal.length !== 8) return;
            $scope.loadingPersonal = true;
            $scope.editCuenta = false;
            $http.post('/api/index.php/personal', data).then(function (response) {
                ContentData.setDataUsuarios(newReadPersonal(response.data));
                $timeout(function () {
                    $scope.loadingPersonal = false;
                    message($mdToast, 'Personal registrado satisfactoriamente');
                    $scope.editPersonal = 'Nuevo';
                    $scope.personalItem.selectedIndex = 0;
                }, 1500);
            }).catch(function (response) {
                message($mdToast, 'Ha ocurrido un Error');
                $scope.loadingPersonal = false;
            });
        }

        $scope.Update_Personal = function (data) {
            if (data.dni_personal.length !== 8) return;
            $scope.loadingPersonal = true;
            $scope.editCuenta = true;
            $http.put('/api/index.php/personal/' + data.dni_personal, data).then(function (response) {
                ContentData.setDataUsuarios(newReadPersonal(response.data));
                $timeout(function () {
                    $scope.loadingPersonal = false;
                    message($mdToast, 'Personal actualizado satisfactoriamente');
                }, 1500);
            }).catch(function (response) {
                message($mdToast, 'Ha ocurrido un Error');
                $scope.loadingPersonal = false;
            });
        }

        $scope.Save_Cuenta = function (data) {
            if (data.dni_personal.length !== 8) return;
            $scope.loadingPersonal = true;
            $http.post('/api/index.php/user', data).then(function (response) {
                ContentData.setDataAccount(response.data);
                $http.get('/api/index.php/personal').then(response => {
                    ContentData.setDataUsuarios(newReadPersonal(response.data));
                }).catch(function (response) {
                    message($mdToast, 'Ha ocurrido un Error');
                    $scope.loadingPersonal = false;
                });
                $timeout(function () {
                    $scope.loadingPersonal = false;
                    message($mdToast, 'Cuenta registrada satisfactoriamente');
                }, 1500);
            }).catch(function (response) {
                message($mdToast, 'Ha ocurrido un Error');
                $scope.loadingPersonal = false;
            });
        }

        function newReadPersonal(array) {
            return array.map(value => {
                value.account = ContentData.getDataAccount().filter(val => val.dni_personal === value.dni_personal).length > 0 ? true : false;
                return value;
            });
        }

    });