'use strict';
angular.module('clientApp')
  .controller('ClienteCtrl', function ($scope, ContentData, $http, $mdToast, $mdDialog, $log) {

    $scope.$watch(function () { return ContentData.getDataCliente(); }, function (value) {
      if (value !== undefined) {
        $scope.Collection_cliente = value;
      }
    });

    $scope.passHistorial=function(item){
      $scope.historialClient=true;
      $scope.clienteItem.selectedIndex = 1;
      $scope.detailsClient={info:item,historial:ContentData.getDataActa().filter(value=>value.dni_cliente === item.dni_cliente)};
    }


  });