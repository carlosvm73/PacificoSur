'use strict';
angular.module('clientApp')
  .controller('ProveedorCtrl', function ($scope, ContentData, $http, $mdToast, $timeout, $mdDialog, $log) {

    $scope.$watch(function () {
      return ContentData.getDataProveedor();
    }, function (value) {
      if (value !== undefined) {
        $scope.Collection_proveedor = value;
      }
    });

    $scope.cancerToEditProveedor = function () {
      $scope.editProveedor = 'Nuevo';
      $scope.proveedorItem.selectedIndex = 0;
      $scope.proveedor = {};
    }

    $scope.Pass_Proveedor = function (op, data, ev) {
      if (op === 'modify') {
        $scope.editProveedor = 'Editar';
        $scope.proveedorItem.selectedIndex = 1;
        $scope.proveedor = data;
      } else if (op === 'delete') {


      $http.get('/api/index.php/acta').then(function (response) {
        console.log(response);
        if(response.data !== undefined || response.data !== ''){
          let id=response.data.filter(value=>value.idProveedor===data.idProveedor);
          if(id.length > 0){
            $timeout(function () {
              message($mdToast, 'Este proveedor contiene datos y esta relacionada; no puede ser eliminada !!!');
            }, 1500);
          }else{
            var confirm = $mdDialog.confirm()
              .title(`Desea Eliminar el Proveedor ${data.nombres_proveedor}?`)
              .textContent('Este proveedor no contiene relacion alguna; por tanto puede ser eliminada')
              .ariaLabel('proveedor')
              .targetEvent(ev)
              .ok('Eliminar')
              .cancel('Cancelar');

            $mdDialog.show(confirm).then(function () {

              $scope.loadingProveedor = true;
              $http.delete('/api/index.php/proveedor/' + data.idProveedor).then(function (response) {
                ContentData.setDataProveedor(response.data);
                $timeout(function () {
                  $scope.loadingProveedor = false;
                  message($mdToast, 'Proveedor eliminado satisfactoriamente');
                }, 1500);
              }).catch(function (response) {
                message($mdToast, 'Ha ocurrido un Error');
                $scope.loadingProveedor = false;
              });

            }, function () {
              $log.info('Operacion cancelada');
            });
          }
        }
      }).catch(function (response) {
        message($mdToast, 'Ha ocurrido un Error');
      });


      } else {

      }
    }

    $scope.Save_Proveedor = function (data) {
      if(data.ruc_proveedor.length !== 11) return;
      $scope.loadingProveedor = true;
      $http.post('/api/index.php/proveedor', data).then(function (response) {
        ContentData.setDataProveedor(response.data);
        $timeout(function () {
          $scope.proveedor = {};
          message($mdToast, 'Proveedor registrado exitosamente');
          $scope.proveedorItem.selectedIndex = 0;
          $scope.loadingProveedor = false;
        }, 1500);
      }).catch(function (response) {
        message($mdToast, 'Ha ocurrido un Error');
        $scope.proveedorItem.selectedIndex = 0;
        $scope.loadingProveedor = false;
      });
    }

    $scope.Update_Proveedor = function (data) {
      if(data.ruc_proveedor.length !== 11) return;      
      $scope.loadingProveedor = true;
      $http.put('/api/index.php/proveedor/' + data.idProveedor, {
        direccion_proveedor: data.direccion_proveedor,
        idProveedor: data.idProveedor,
        nombres_proveedor: data.nombres_proveedor,
        primer_apellido_proveedor: data.primer_apellido_proveedor,
        ruc_proveedor: data.ruc_proveedor,
        segundo_apellido_proveedor: data.segundo_apellido_proveedor,
      }).then(function (response) {
        ContentData.setDataProveedor(response.data);
        $timeout(function () {
          $scope.proveedor = {};
          message($mdToast, 'Proveedor actualizado exitosamente');
          $scope.proveedorItem.selectedIndex = 0;
          $scope.loadingProveedor = false;
        }, 1500);
      }).catch(function (response) {
        message($mdToast, 'Ha ocurrido un Error');
        $scope.proveedorItem.selectedIndex = 0;
        $scope.loadingProveedor = false;
      });
      $scope.editProveedor = 'Nuevo';

    }

  });