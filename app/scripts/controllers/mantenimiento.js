'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:MantenimientoCtrl
 * @description
 * # MantenimientoCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('MantenimientoCtrl', function ($scope, ContentData, $http, $mdToast, $timeout, $mdDialog, $log) {

    $scope.$watch(function () {
      return ContentData.getDataMantenimiento();
    }, function (value) {
      if (value !== undefined) {
        $scope.Collection_mantenimiento = value.map(value => {
          value.Area = ContentData.getDataArea().filter(val => val.idArea === value.idArea)[0].descripcion_area;
          value.Cargo = ContentData.getDataCargo().filter(val => val.idCargo === value.idCargo)[0].descripcion_cargo;
          return value;
        });
      }
    });
    $scope.$watch(function () {
      return ContentData.getDataArea();
    }, function (value) {
      if (value !== undefined) {
        $scope.listArea = value;
      }
    });

    $scope.$watch(function () {
      return ContentData.getDataUsuarios();
    }, function (value) {
      if (value !== undefined) {
        $scope.listPersonal = value;
      }
    });

    $scope.cancerToEditMantenimiento = function () {
      $scope.editIncidencias = true;
      $scope.mantenimiento = {};
      $scope.mantenimientoItem.selectedIndex = 0;
      $scope.editMantenimiento = 'Nuevo';
    }
    $scope.cancerToEditIncidencias = function () {
      $scope.mantenimientoItem.selectedIndex = 0;
      $scope.seeIncidente = false;
      $scope.editIncidencias = 'Nuevo';
    }

    $scope.Pass_Mantenimiento = function (op, item, ev) {
      if (op === 'modify') {
        $scope.editIncidencias = true;
        $scope.mantenimiento = item;
        $scope.mantenimientoItem.selectedIndex = 1;
        $scope.editMantenimiento = 'Editar';
      } else if (op === 'incidente') {
        $scope.incidencias={idMantenimiento:item.idMantenimiento};
        $scope.mantenimientoItem.selectedIndex = 1;
        $scope.seeIncidente = true;
        $scope.editIncidencias = 'Editar';

      } else if (op === 'historial') {
        $scope.viewIncedencias = true;
        item.historial = ContentData.getDataIncidente().filter(value => value.idMantenimiento === item.idMantenimiento);
        $scope.detailsMatenimiento = item;
        $scope.mantenimientoItem.selectedIndex = 2;
      } else if (op === 'delete') {
        var confirm = $mdDialog.confirm()
          .title(`Desea Eliminar el Matenimiento ${item.idMantenimiento}?`)
          .ariaLabel('pedido')
          .targetEvent(ev)
          .ok('Eliminar')
          .cancel('Cancelar');

        $mdDialog.show(confirm).then(function () {

          $scope.loadingMantenimiento = true;
          $http.delete('/api/index.php/mantenimiento/' + item.idMantenimiento).then(function (response) {
            ContentData.setDataMantenimiento(response.data);
            $timeout(function () {
              $scope.loadingMantenimiento = false;
              message($mdToast, 'Mantenimiento eliminado satisfactoriamente');
            }, 1500);
          }).catch(function (response) {
            message($mdToast, 'Ha ocurrido un Error');
            $scope.loadingMantenimiento = false;
          });

        }, function () {
          $log.info('Operacion cancelada');
        });

      }

    }

    $scope.Save_Mantenimiento = function (data) {
      $scope.loadingMantenimiento = true;
      $scope.editIncidencias = false;
      $http.post('/api/index.php/mantenimiento', data).then(function (response) {
        ContentData.setDataMantenimiento(response.data);
        $timeout(function () {
          $scope.loadingMantenimiento = false;
          message($mdToast, 'Matenimiento registrado satisfactoriamente');
        }, 1500);
      }).catch(function (response) {
        message($mdToast, 'Ha ocurrido un Error');
        $scope.loadingMantenimiento = false;
      });
    }

    $scope.Update_Mantenimiento = function (data) {
      $scope.loadingMantenimiento = true;
      $scope.editIncidencias = true;
      $http.put('/api/index.php/mantenimiento/' + data.idMantenimiento, data).then(function (response) {
        ContentData.setDataMantenimiento(response.data);
        $timeout(function () {
          $scope.loadingMantenimiento = false;
          message($mdToast, 'Mantenimiento actualizado satisfactoriamente');
        }, 1500);
      }).catch(function (response) {
        message($mdToast, 'Ha ocurrido un Error');
        $scope.loadingMantenimiento = false;
      });
    }

    $scope.Save_Incidencias = function (data) {
      $scope.loadingMantenimiento = true;
      $http.post('/api/index.php/incidente', data).then(function (response) {
        ContentData.setDataIncidente(response.data);
        $timeout(function () {
          $scope.loadingMantenimiento = false;
          message($mdToast, 'Incidente Tecnico registrado satisfactoriamente');
        }, 1500);
      }).catch(function (response) {
        message($mdToast, 'Ha ocurrido un Error');
        $scope.loadingMantenimiento = false;
      });
    }

  });