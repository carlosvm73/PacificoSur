'use strict';
angular.module('clientApp')

  .directive('mdCliente', function () {
    return {
      controller: 'ClienteCtrl',
      templateUrl: 'views/forms/cliente.html',
      restrict: 'E'
    };
  })
  .directive('mdConfiguracion', function () {
    return {
      controller: 'ConfiguracionCtrl',
      templateUrl: 'views/forms/configuracion.html',
      restrict: 'E'
    };
  })
  .directive('mdConformidad', function () {
    return {
      controller: 'ConformidadCtrl',
      templateUrl: 'views/forms/conformidad.html',
      restrict: 'E'
    };
  })
  .directive('mdDashboard', function () {
    return {
      controller: 'DashboardCtrl',
      templateUrl: 'views/forms/dashboard.html',
      restrict: 'E'
    };
  })
  .directive('mdMantenimiento', function () {
    return {
      templateUrl: 'views/forms/mantenimiento.html',
      restrict: 'E',
      controller: 'MantenimientoCtrl'
    };
  })
  .directive('mdPatrimonio', function () {
    return {
      controller: 'PatrimonioCtrl',
      templateUrl: 'views/forms/patrimonio.html',
      restrict: 'E'
    };
  })
  .directive('mdPedidos', function () {
    return {
      controller: 'PedidosCtrl',
      templateUrl: 'views/forms/pedidos.html',
      restrict: 'E'
    };
  })
  .directive('mdPersonal', function () {
    return {
      templateUrl: 'views/forms/personal.html',
      controller: 'PersonalCtrl',
      restrict: 'E'
    };
  })
  .directive('mdProveedor', function () {
    return {
      controller: 'ProveedorCtrl',
      templateUrl: 'views/forms/proveedor.html',
      restrict: 'E'
    };
  })
  .directive('mdRequerimiento', function () {
    return {
      controller: 'RequerimientoCtrl',
      templateUrl: 'views/forms/requerimiento.html',
      restrict: 'E'
    };
  });

