'use strict';

/**
 * @ngdoc overview
 * @name appApp
 * @description
 * # clientApp
 *
 * Main module of the application.
 */
angular
  .module('clientApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngMaterial',
    'ngMdIcons',
    'htmlToPdfSave'
  ])
  .run(function($rootScope, $cookieStore, $location, $log, $http){
    $rootScope.$on('$routeChangeStart', function(event, next, current){
      if ($cookieStore.get('token') === undefined || $cookieStore.get('token')===null) {
        if (next.templateUrl==='views/home.html') {
          $location.path('/');
        }
      }else{
        $location.path('/home');
        if (next.templateUrl==='views/login.html' || next.templateUrl==='views/login.html') {
          $location.path('/home');
        }
      }
    })
  })
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/home', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl',
        controllerAs: 'home'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .config(function($mdThemingProvider) {
  $mdThemingProvider.definePalette('Indigo', {
   '50': 'e8eaf6',
   '100': 'c5cae9',
   '200': '9fa8da',
   '300': '7986cb',
   '400': '5c6bc0',
   '500': '3f51b5',
   '600': '3949ab',
   '700': '303f9f',
   '800': '283593',
   '900': '1a237e',
   'A100': '8c9eff',
   'A200': '536dfe',
   'A400': '3d5afe',
   'A700': '304ffe',
   'contrastDefaultColor': 'light',

   'contrastDarkColors': ['50', '100',
   '200', '300', '400', 'A100'],
   'contrastLightColors': undefined
 });
  $mdThemingProvider.definePalette('state', {
   '50': '0288d1',
   '100': 'd32f2f',
   '200': '43a047',
   '300': 'e65100',
   '400': '6d4c41',
   '500': '3f51b5',
   '600': '3949ab',
   '700': '303f9f',
   '800': '283593',
   '900': '1a237e',
   'A100': '8c9eff',
   'A200': '536dfe',
   'A400': '3d5afe',
   'A700': '304ffe',
   'contrastDefaultColor': 'light',

   'contrastDarkColors': ['50', '100',
   '200', '300', '400', 'A100'],
   'contrastLightColors': undefined
 });
  $mdThemingProvider.definePalette('Grey', {
   '50': 'fafafa',
   '100': 'f5f5f5',
   '200': 'eeeeee',
   '300': 'e0e0e0',
   '400': 'bdbdbd',
   '500': '9e9e9e',
   '600': '757575',
   '700': '616161',
   '800': '424242',
   '900': '212121',
   'A100': '212121',
   'A200': '212121',
   'A400': '212121',
   'A700': '212121',
   'contrastDefaultColor': 'light',

   'contrastDarkColors': ['50', '100',
   '200', '300', '400', 'A100'],
   'contrastLightColors': undefined
 });
  $mdThemingProvider.definePalette('Light Blue', {
   '50': 'e1f5fe',
   '100': 'b3e5fc',
   '200': '81d4fa',
   '300': '4fc3f7',
   '400': '29b6f6',
   '500': '03a9f4',
   '600': '039be5',
   '700': '0288d1',
   '800': '0277bd',
   '900': '01579b',
   'A100': '80d8ff',
   'A200': '40c4ff',
   'A400': '00b0ff',
   'A700': '0091ea',
   'contrastDefaultColor': 'light',

   'contrastDarkColors': ['50', '100',
   '200', '300', '400', 'A100'],
   'contrastLightColors': undefined
 });
  $mdThemingProvider.definePalette('Deep Purple', {
   '50': 'ede7f6',
   '100': 'd1c4e9',
   '200': 'b39ddb',
   '300': '9575cd',
   '400': '7e57c2',
   '500': '673ab7',
   '600': '5e35b1',
   '700': '512da8',
   '800': '4527a0',
   '900': '311b92',
   'A100': 'b388ff',
   'A200': '7c4dff',
   'A400': '651fff',
   'A700': '6200ea',
   'contrastDefaultColor': 'light',

   'contrastDarkColors': ['50', '100',
   '200', '300', '400', 'A100'],
   'contrastLightColors': undefined
 });
  $mdThemingProvider.definePalette('red', {
   '50' :'ffebee',
   '100' :'ffcdd2',
   '200' :'ef9a9a',
   '300' :'e57373',
   '400' :'ef5350',
   '500' :'f44336',
   '600' :'e53935',
   '700' :'d32f2f',
   '800' :'c62828',
   '900' :'b71c1c',
   'A100' :'ff8a80',
   'A200' :'ff5252',
   'A400' :'ff1744',
   'A700' :'d50000',
   'contrastDefaultColor': 'light',

   'contrastDarkColors': ['50', '100',
   '200', '300', '400', 'A100'],
   'contrastLightColors': undefined
 });

  $mdThemingProvider.definePalette('ambar', {
   '50' :'fff8e1',
   '100' :'ffecb3',
   '200' :'ffe082',
   '300' :'ffd54f',
   '400' :'ffca28',
   '500' :'ffc107',
   '600' :'ffb300',
   '700' :'ffa000',
   '800' :'ff8f00',
   '900' :'ff6f00',
   'A100' :'ffe57f',
   'A200' :'ffd740',
   'A400' :'ffc400',
   'A700' :'ffab00',
   'contrastDefaultColor': 'light',

   'contrastDarkColors': ['50', '100',
   '200', '300', '400', 'A100'],
   'contrastLightColors': undefined
 });
  $mdThemingProvider.definePalette('green', {
   '50' :'e8f5e9',
   '100' :'c8e6c9',
   '200' :'a5d6a7',
   '300' :'81c784',
   '400' :'66bb6a',
   '500' :'4caf50',
   '600' :'43a047',
   '700' :'388e3c',
   '800' :'2e7d32',
   '900' :'1b5e20',
   'A100' :'b9f6ca',
   'A200' :'69f0ae',
   'A400' :'00e676',
   'A700' :'00c853',
   'contrastDefaultColor': 'light',

   'contrastDarkColors': ['50', '100',
   '200', '300', '400', 'A100'],
   'contrastLightColors': undefined
 });
  $mdThemingProvider.definePalette('green', {
   '50' :'f1f8e9',
   '100' :'dcedc8',
   '200' :'c5e1a5',
   '300' :'aed581',
   '400' :'9ccc65',
   '500' :'8bc34a',
   '600' :'7cb342',
   '700' :'689f38',
   '800' :'558b2f',
   '900' :'33691e',
   'A100' :'ccff90',
   'A200' :'b2ff59',
   'A400' :'76ff03',
   'A700' :'64dd17',
   'contrastDefaultColor': 'light',

   'contrastDarkColors': ['50', '100',
   '200', '300', '400', 'A100'],
   'contrastLightColors': undefined
 });

  $mdThemingProvider.definePalette('deep-purple', {
   '50' :'ede7f6',
   '100' :'d1c4e9',
   '200' :'b39ddb',
   '300' :'9575cd',
   '400' :'7e57c2',
   '500' :'673ab7',
   '600' :'5e35b1',
   '700' :'512da8',
   '800' :'4527a0',
   '900' :'311b92',
   'A100' :'b388ff',
   'A200' :'7c4dff',
   'A400' :'651fff',
   'A700' :'6200ea',
   'contrastDefaultColor': 'light',

   'contrastDarkColors': ['50', '100',
   '200', '300', '400', 'A100'],
   'contrastLightColors': undefined
 });

  $mdThemingProvider.definePalette('tuqueza', {
   '50' :'ede7f6',
   '100' :'d1c4e9',
   '200' :'b39ddb',
   '300' :'9575cd',
   '400' :'7e57c2',
   '500' :'673ab7',
   '600' :'5e35b1',
   '700' :'512da8',
   '800' :'4527a0',
   '900' :'00B4CE',
   'A100' :'b388ff',
   'A200' :'7c4dff',
   'A400' :'651fff',
   'A700' :'6200ea',
   'contrastDefaultColor': 'light',

   'contrastDarkColors': ['50', '100',
   '200', '300', '400', 'A100'],
   'contrastLightColors': undefined
 });

  $mdThemingProvider.definePalette('morado', {
   '50' :'ede7f6',
   '100' :'d1c4e9',
   '200' :'b39ddb',
   '300' :'9575cd',
   '400' :'7e57c2',
   '500' :'673ab7',
   '600' :'5e35b1',
   '700' :'512da8',
   '800' :'4527a0',
   '900' :'4E4184',
   'A100' :'b388ff',
   'A200' :'7c4dff',
   'A400' :'651fff',
   'A700' :'6200ea',
   'contrastDefaultColor': 'light',

   'contrastDarkColors': ['50', '100',
   '200', '300', '400', 'A100'],
   'contrastLightColors': undefined
 });

  $mdThemingProvider.definePalette('azul', {
   '50' :'ede7f6',
   '100' :'d1c4e9',
   '200' :'b39ddb',
   '300' :'9575cd',
   '400' :'7e57c2',
   '500' :'673ab7',
   '600' :'5e35b1',
   '700' :'512da8',
   '800' :'4527a0',
   '900' :'174183',
   'A100' :'b388ff',
   'A200' :'7c4dff',
   'A400' :'651fff',
   'A700' :'6200ea',
   'contrastDefaultColor': 'light',

   'contrastDarkColors': ['50', '100',
   '200', '300', '400', 'A100'],
   'contrastLightColors': undefined
 });

  $mdThemingProvider.definePalette('pantone-verde', {
   '50' :'ede7f6',
   '100' :'d1c4e9',
   '200' :'b39ddb',
   '300' :'9575cd',
   '400' :'7e57c2',
   '500' :'673ab7',
   '600' :'5e35b1',
   '700' :'512da8',
   '800' :'4527a0',
   '900' :'A5D6CD',
   'A100' :'b388ff',
   'A200' :'7c4dff',
   'A400' :'651fff',
   'A700' :'6200ea',
   'contrastDefaultColor': 'light',

   'contrastDarkColors': ['50', '100',
   '200', '300', '400', 'A100'],
   'contrastLightColors': undefined
 });


  $mdThemingProvider.theme('default')
  .primaryPalette('Indigo',{
   'hue-3':'50'
 })
  .accentPalette('pink')
  .warnPalette('orange');

});
