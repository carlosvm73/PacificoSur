'use strict';

/**
 * @ngdoc service
 * @name clientApp.resourse
 * @description
 * # resourse
 * Factory in the clientApp.
 */
angular.module('clientApp')
  .factory('resource', function ($resource) {
    let personalContent = [];
    var meaningOfLife = 0;
    return {
      // setPersonal: function () {
      //   return meaningOfLife;
      // },
      getPersonal: function () {
        return $resource('http://localhost/devApp/api/index.php/api/personal/:num',{num:'@num'});
        // return meaningOfLife;
      }
    };
  });

  // angular.module('clientApp')
  // .factory('resource', function ($resource, $q) {
  //   let personalContent = [];
  //   var meaningOfLife = 0;
  //   return {
  //     setPersonal: function (id) {
  //       return $q((resolve, reject)=>{
  //         if(){

  //         }
  //       });
  //     },
  //     getPersonal: function (id) {
  //       return $q((resolve, reject)=>{
  //         $resource('http://localhost/devApp/api/index.php/api/personal/:num',{num:'@num'});
  //       });
  //       // return meaningOfLife;
  //     }
  //   };
  // });